var q = 1;
if (typeof document.getElementsByClassName("c-inputbox")[0] != 'undefined') {
	var xmlhttp;
	xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			q = xmlhttp.responseText;
			loadLesson(q);
		}
	}
	xmlhttp.open("GET","getUserProgress.php",true);
	xmlhttp.send();
}
		var canProceed = true;
		var pastInput = [];
		var pastInputCurId = 0;
		var userCode = [];
		var lineNum;
		var curHint = 0;
		var curProgress = 0;
		var answer;
		var newErr = [];
		var modalToggle = false;
		var lessonType = "Text";
		var lessonCat;
		var userInputHistory = new Array;
		var curPos;
		var codeMirrors = [];
		function keyup(e) {
			if (e.keyCode == 13) {
				userInputHistory.push(document.getElementsByClassName("c-inputbox")[0].value);
				curPos = userInputHistory.length;
				userInput = document.getElementsByClassName("c-inputbox")[0].value;
				if (!/(\$)/.test(userInput)) {
					if (canProceed) {
						if (document.getElementsByClassName("c-inputbox")[0].value == 'next;') {
							document.getElementsByClassName("c-content")[0].innerHTML += "<div class='c-output'>> "+document.getElementsByClassName("c-inputbox")[0].value+"</div>";
							document.getElementsByClassName("c-content")[0].innerHTML += "<div class='c-output'>Loading Next Lesson...</div>";
							q++;
							loadLesson(q);
							document.getElementsByClassName("c-inputbox")[0].value = "";
							return;
						} else {
							if (document.getElementsByClassName("c-inputbox")[0].value.toLowerCase().indexOf('next') != -1) {
								document.getElementsByClassName("c-content")[0].innerHTML += "<div class='c-output error'>ReferenceError: "+document.getElementsByClassName("c-inputbox")[0].value+" is not defined. Did you mean next;<div>";
								document.getElementsByClassName("c-inputbox")[0].value = "";
								return;
							}
							document.getElementsByClassName("c-content")[0].innerHTML += "<div class='c-output error'>ReferenceError: "+document.getElementsByClassName("c-inputbox")[0].value+" is not defined</div>";
							document.getElementsByClassName("c-inputbox")[0].value = "";
							return;
						}
					} else {
						if (userInput == "$ getHint") {
							if (curHint == 3) {
								document.getElementsByClassName("c-content")[0].insertBefore(newElement("div", "c-output error", "You are only allowed 3 hints"), document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
								document.getElementsByClassName("c-inputbox")[0].value = "";
								return;
							}
							document.getElementsByClassName("c-content")[0].insertBefore(newElement("div", "c-output", "Loading Hint "+(curHint+1)), document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
							getHint(q);
							document.getElementsByClassName("c-inputbox")[0].value = "";
						}
						var objDiv = document.getElementsByClassName("console")[0];
						objDiv.scrollTop = objDiv.scrollHeight;
						//console.log(1);
						return;
					}
				} else {
					if (!canProceed) {
						if (/^\$ (node)/.test(document.getElementsByClassName("c-inputbox")[0].value)) {
							if (/(^\$ node [a-zA-Z0-9-_]*)/.test(document.getElementsByClassName("c-inputbox")[0].value)) {
								if (/(^\$ node [a-zA-Z0-9-_]*.js)/.test(document.getElementsByClassName("c-inputbox")[0].value)) {
									
									var errLines = document.getElementsByClassName("CodeMirror-code")[document.getElementsByClassName("CodeMirror-code").length-1].childNodes;
									for (var i = 0; i < errLines.length; i++) {
										errLines[i].className = "";
									}
									
/* 									var codeEditors = document.getElementsByClassName("CodeEditor");
									var curEditor = codeEditors[codeEditors.length-1];
									curEditor = curEditor.nextSibling;
									var userCode = curEditor.childNodes[5];
									var userCode = textToArray(userCode.innerText);
									console.log(codeMirrors); */
									for (var i = 0; i < codeMirrors.length; i++) {
										if (codeMirrors[i].getTextArea().className == "CodeEditor") {
											userCode = codeMirrors[i].getValue().split("\n");
										}
									}
									if (userCode.length != 1) {
										if (/^.*(listen\s*\(\d*\)).*/.test(userCode.join())) {
											if (/^.*(listen\s*\((80)\)).*/.test(userCode.join()) || /^.*(listen\s*\((0)\)).*/.test(userCode.join())) {
												document.getElementsByClassName("c-content")[0].insertBefore(newElement("div", "c-output error", "You may not use port 0 or 80"), document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
											} else {
												if (/\w.*\.write\(\d{1,}\).*/g.test(userCode.join())) {
													document.getElementsByClassName("c-content")[0].insertBefore(newElement("div", "c-output error", "res.write() may only be a variable or a string."), document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
												} else {
													if (/(\w.*\.end\(.*\).*)/.test(userCode.join())) {
														document.getElementsByClassName("c-content")[0].insertBefore(newElement("div", "c-output", "Please wait while we test your script..."), document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
														var xmlhttp;
														xmlhttp=new XMLHttpRequest();
														xmlhttp.onreadystatechange=function()
														{
															if (xmlhttp.readyState==2 && xmlhttp.status==200) {
																changeProgress(50);
															}
															if (xmlhttp.readyState==4 && xmlhttp.status==200)
															{
																changeProgress(50);
																setTimeout(function() {
																	//fadeOut(500, document.getElementsByClassName("meter")[document.getElementsByClassName("meter").length-1]);
																	 
																	var result = xmlhttp.responseText.trim();

																	console.log(result);
																	if (result == answer) {
																		console.log("Correct");
																		document.getElementsByClassName("c-content")[0].innerHTML += "<div class='c-output'>Your solution was correct. Please type 'next;' and press enter to continue.</div>";
																		//document.getElementsByClassName("c-content")[0].insertAfter(newElement("div", "c-output", "Your solution was correct. Please type 'next;' and press enter to continue."), document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
																		document.getElementsByClassName("c-inputbox")[0].value == "";
																		canProceed = true;
																		curProgress = 0;
																	} else {
																		if (result.indexOf("<:::>") != -1) {
																			var err = result.split("<:::>");
																			console.log(err);
																			if (err[0] == "340") {
																				var missingModule = err[1].match(/Error: Cannot find module '(.*)'/);
																				console.log(missingModule);
																				var searchRegex = new RegExp("require\\([\"\']("+missingModule[1]+")[\"\']\\)");
																				if (arraySearch(userCode, searchRegex, err))
																				err = newErr;
																			}
																			
																			 //get error element
																			var errLines = document.getElementsByClassName("CodeMirror-code")[document.getElementsByClassName("CodeMirror-code").length-1].childNodes;
																			
																			errLines[err[0]-1].className = "lineError"; 
																			
																			//document.getElementsByClassName("L"+(err[0]-1))[document.getElementsByClassName("L"+(err[0]-1)).length-1].className = "L0 lineError";
																			
																			document.getElementsByClassName("c-content")[0].insertBefore(newElement("div", "c-output error", err[1]+" on line "+err[0]),document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
																			
																		} else {
																			document.getElementsByClassName("c-content")[0].insertBefore(newElement("div", "c-output error", "Your solution was incorrect. Please edit your code and try again."), document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
																			console.log(err);
																		}
																		curProgress = 0;
																	}
																}, 1000);

																
															}
														}
														console.log("executeScript.php?a="+JSON.stringify(userCode));
														xmlhttp.open("GET","executeScript.php?a="+JSON.stringify(userCode),true);
														xmlhttp.send();
													} else {
														document.getElementsByClassName("c-content")[0].insertBefore(newElement("div", "c-output error", "Missing res.end(). You must end your response."), document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
													}
												}
											}
										} else {
											document.getElementsByClassName("c-content")[0].insertBefore(newElement("div", "c-output error", "You need to specify a listening port"), document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
										}
									} else {
										document.getElementsByClassName("c-content")[0].insertBefore(newElement("div", "c-output error", "Your script is empty"), document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
									}
								} else {
									document.getElementsByClassName("c-content")[0].insertBefore(newElement("div", "c-output error", "The script must have the extension .js"), document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
								}
							} else {
								document.getElementsByClassName("c-content")[0].insertBefore(newElement("div", "c-output error", "You must define a script to run"), document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
							}
						} else if(userInput == "$ getHint") {
							if (curHint == 3) {
								document.getElementsByClassName("c-content")[0].insertBefore(newElement("div", "c-output error", "You are only allowed 3 hints"), document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
								document.getElementsByClassName("c-inputbox")[0].value = "";
								return;
							}
							document.getElementsByClassName("c-content")[0].insertBefore(newElement("div", "c-output", "Loading Hint "+(curHint+1)), document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
							getHint(q);
							document.getElementsByClassName("c-inputbox")[0].value = "";
						} else {
							if (document.getElementsByClassName("c-inputbox")[0].value == "$") {
								document.getElementsByClassName("c-content")[0].insertBefore(
								newElement("div", "c-output error", "'' is not recognized as an internal or external command, operable program or batch file."),
								document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
							} else {
								document.getElementsByClassName("c-content")[0].insertBefore(
								newElement("div", "c-output error", "'"+ document.getElementsByClassName("c-inputbox")[0].value.match(/^\$ (\w*)/)[1]+"' is not recognized as an internal or external command, operable program or batch file."),
								document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
							}
						}
					}
					//console.log(document.getElementsByClassName("c-inputbox")[0].value);
					if (/^(\$ cookies)/.test(document.getElementsByClassName("c-inputbox")[0].value)) {
						document.getElementsByClassName("c-content")[0].innerHTML += "<div class='c-output'>OM NOM NOM they're are yummy</div>";
					}
					if (/^(\$ cake)/.test(document.getElementsByClassName("c-inputbox")[0].value)) {
						document.getElementsByClassName("c-content")[0].innerHTML += "<div class='c-output'>It's a lie</div>";
					}
					if (/^(\$ dinosaur)/.test(document.getElementsByClassName("c-inputbox")[0].value)) {
						document.getElementsByClassName("c-content")[0].innerHTML += "<div class='c-output'>Rawrr</div>";
					}
					document.getElementsByClassName("c-inputbox")[0].value = "";
					var objDiv = document.getElementsByClassName("console")[0];
					objDiv.scrollTop = objDiv.scrollHeight;
					console.log(2);
				}
			}
			if (e.keyCode == 38) {
				curPos--;
				if (curPos == -1) {
					curPos = userInputHistory.length-1;
				}
				document.getElementsByClassName("c-inputbox")[0].value = userInputHistory[curPos];
			}
			if (e.keyCode == 40) {
				curPos++;
				document.getElementsByClassName("c-inputbox")[0] = userInputHistory[curPos];
				if (curPos == userInputHistory.length) {
					document.getElementsByClassName("c-inputbox")[0].value = "";
				}
			}
		}

		function getHint(i) {
			var xmlhttp;
			xmlhttp=new XMLHttpRequest();
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var hints = xmlhttp.responseText.split('<:::>');
					curHint++;
					document.getElementsByClassName("c-content")[0].insertBefore(newElement("div", "c-log hint", ""), document.getElementsByClassName("CodeEditor")[document.getElementsByClassName("CodeEditor").length-1]);
					document.getElementsByClassName("hint")[document.getElementsByClassName("hint").length-1].innerHTML = hints[curHint-1];
					//prettyPrint();
					
					var hints = document.getElementsByClassName("hint").length-1
					var topPos = document.getElementsByClassName("hint")[hints].offsetTop;
					document.getElementsByClassName("console")[0].scrollTop = topPos-50;
					//console.log(3);
					
				}
			}
			xmlhttp.open("GET","loadHint.php?i="+i,true);
			xmlhttp.send();
		}

		function loadLesson(i) {
			userCode = [];
			document.getElementsByClassName("c-inputbox")[0].disabled = true;
			if (lessonType == "Code") {
				if (typeof document.getElementsByClassName("searchSide")[0] != "undefined") {
					console.log((+lessonCat+1));
					document.getElementById("forumCat-"+(+lessonCat+1)).childNodes[0].className = "";
					document.getElementById("forumCat-"+(+lessonCat+1)).childNodes[0].onclick = function() { toggleModal(this.parentNode, 'forum') };
					
				}
			}
			var xmlhttp;
			xmlhttp=new XMLHttpRequest();
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var lesson = xmlhttp.responseText.split('<:::>');
					if (lesson[0] == "") {
						document.getElementsByClassName("c-content")[0].innerHTML += "<div class='c-log'>Lesson does not exist</div>";
						return;
					}
					var lessonName = lesson[0];
					var lessonContent = lesson[1];
					lessonType = lesson[2];
					if (lessonType == "Code") {
						canProceed = false;
						answer = lesson[3];
					}
					lessonCat = lesson[4];
					var lessonID = lesson[5];
					if (document.getElementsByClassName("searchSide").length > 0 && lesson[0].indexOf("There are no more lessons") == -1) {
						var el = document.getElementById("cat"+lessonCat).childNodes[1];
						var heads = document.getElementsByClassName("accHead");
						for (var i = 0; i < heads.length; i++) {
							var head = heads[i].childNodes[1];
							head.removeAttribute("style");
							head.childNodes[1].removeAttribute("style");
						}
						var subMenus = document.getElementsByClassName("sub-menu");
						for (var i = 0; i < subMenus.length; i++) {
							subMenus[i].removeAttribute("style");
							var subMenu = subMenus[i];
							var subLis = subMenu.getElementsByTagName("li");
							for (var y = 0; y < subLis.length; y++) {
								subLis[y].childNodes[0].removeAttribute("style");
							}
						}
						var curLess = document.getElementById(lessonCat+"-"+lessonID).childNodes[0];
						curLess.style.backgroundColor = "#CAD6B0";
						curLess.style.color = "#1F1F1F";
						curLess.style.textShadow = "1px 1px 1px rgba(255,255,255, .2)";
						
						el.style.backgroundColor = "#a5cd4e";
						el.style.color = "#1F1F1F";
						el.style.textShadow = "1px 1px 1px rgba(255,255,255, .2)";
						el.childNodes[1].style.backgroundColor = "#3e5706";
						el.childNodes[1].style.color = "#fdfdfd";
						el.childNodes[1].style.textShadow = "0px 1px 0px rgba(0,0,0, .35)";
						var subMenu = el.parentNode.childNodes[3];
						var subItems = subMenu.getElementsByTagName('li');
						subMenuHeight = subItems.length*33;
						subMenu.style.height = subMenuHeight+"px";
					}
					document.getElementsByClassName("c-content")[0].innerHTML += "<div class='c-log'><h3>"+lessonName+"</h3>"+lessonContent+"</div>";
					document.getElementsByClassName("c-inputbox")[0].disabled = false;
					document.getElementsByClassName("c-inputbox")[0].focus();
					
					var heads = document.getElementsByTagName('h3').length-1
					var topPos = document.getElementsByTagName('h3')[heads].offsetTop;
					document.getElementsByClassName('console')[0].scrollTop = topPos-140;
					
					document.title = lessonName+" - Node.JS Academy";
					curHint = 0;
					if (lessonType == "Code") {
						document.getElementsByClassName("c-content")[0].innerHTML += "<textarea class='CodeEditor'>//Write Code In Here</textarea>";
						var codeEditors = document.getElementsByClassName("CodeEditor");
						var myTextArea = codeEditors[codeEditors.length-1];
						var myCodeMirror = CodeMirror.fromTextArea(myTextArea);
						codeMirrors.push(myCodeMirror);
						//tabbulatingUserCode();
					} else {
						var codes = document.getElementsByClassName("prettyprint");
						for (var i = 0; i < codes.length; i++) {
							myCodeMirror = CodeMirror(codes[i], {
								value: codes[i].textContent,
								mode: "javascript"
							});
							codes[i].childNodes[0].textContent = "";
						}
						
					}
//prettyPrint();
					//console.log(document.getElementById(lessonCat+"-"+lessonID).childNodes[0]);
					if (typeof document.getElementsByClassName("searchSide")[0] != "undefined" && lesson[0].indexOf("There are no more lessons") == -1) {
						document.getElementById(lessonCat+"-"+lessonID).childNodes[0].onclick=function() { loadLesson(lessonID); q=lessonID; };
						document.getElementById(lessonCat+"-"+lessonID).childNodes[0].removeAttribute("class");
					}
					if (lesson[0].indexOf("There are no more lessons") != -1) {
						document.getElementsByClassName("c-inputbox")[0].disabled = true;
					}
				}
			}
			xmlhttp.open("GET","loadLesson.php?i="+i,true);
			xmlhttp.send();
		}
		function newElement(el, cn, tn) {
			var newItem=document.createElement(el);
			newItem.className = cn;
			var textnode=document.createTextNode(tn);
			newItem.appendChild(textnode);
			return newItem;
		}

		function changeProgress(a) {
			curProgress+=a;
			//document.getElementsByClassName("meter")[document.getElementsByClassName("meter").length-1].childNodes[0].style.width = curProgress+"%";
		}

		function fadeOut(ms, el) {
		  var opacity = 1,
			interval = 50,
			gap = interval / ms;
			
		  function func() { 
			opacity -= gap;
			el.style.opacity = opacity;
			
			if(opacity <= 0) {
			  window.clearInterval(fading); 
			  el.style.display = 'none';
			}
		  }
		  
		  var fading = window.setInterval(func, interval);

		}

		function multipleOccurs(val) {
			var a,n;
			n = userCode.join('\r\n').match(val);
			console.log(n);
			if(n.length>=2) return true
			return false
		}

		function arraySearch(arr, val, err) {
		console.log(val);
			for (var i=0; i<arr.length; i++) {
				if (val.test(arr[i])) {
					newErr = [i+1, err[1]];
					return true;
				}
				return false;
			}
		}
		function updateUserCode(e) {
			var usercodes = document.getElementsByClassName("CodeEditor");
			var usercode = usercodes[usercodes.length-1];
			if (typeof e != "undefined") {
				if (e.keyCode == 13) {
					usercode.className = "usercode prettyprint";
					//prettyPrint();
				}
			} else {
				usercode.className = "usercode prettyprint";
				//prettyPrint();
			}
		}

		function textToArray(text) {
			var arr = text.split('\n');
			var trimmedArray = [];
			for (var i = 0; i < arr.length; i++) {
				if (arr[i] != "") {
					trimmedArray.push(arr[i]);
				}
			}
			return trimmedArray;
		}

		function keyDown(e) {
			if (e.keyCode == 13) {
				var usercodes = document.getElementsByClassName("CodeEditor");
				var usercode = usercodes[usercodes.length-1];
				var lineNums = usercode.childNodes[0].childNodes;
				if (lineNums[lineNums.length-1].innerText == "\n") {
					e.preventDefault();
				}
			}
		}

		function getCaretOffset(element) {
			var caretOffset = 0;
			var doc = element.ownerDocument || element.document;
			var win = doc.defaultView || doc.parentWindow;
			var sel;
			if (typeof win.getSelection != "undefined") {
				var range = win.getSelection().getRangeAt(0);
				var preCaretRange = range.cloneRange();
				preCaretRange.selectNodeContents(element);
				preCaretRange.setEnd(range.endContainer, range.endOffset);
				caretOffset = preCaretRange.toString().length;
			} else if ( (sel = doc.selection) && sel.type != "Control") {
				var textRange = sel.createRange();
				var preCaretTextRange = doc.body.createTextRange();
				preCaretTextRange.moveToElementText(element);
				preCaretTextRange.setEndPoint("EndToEnd", textRange);
				caretOffset = preCaretTextRange.text.length;
			}
			return caretOffset;
		}

		function setCaretPosition(elem, caretPos) {
		  var char = caretPos;
		  var sel = window.getSelection();
		  sel.collapse(elem.firstChild, char);
		}

		String.prototype.splice = function( idx, rem, s ) {
			return (this.slice(0,idx) + s + this.slice(idx + Math.abs(rem)));
		};
		function showNav(e) {
			if (document.getElementById("resNavDrop").style.maxHeight == "0px" || document.getElementById("resNavDrop").style.maxHeight == "") {
				document.getElementById("resNavDrop").style.maxHeight = "500px";
			} else {
				document.getElementById("resNavDrop").style.maxHeight = "0px";
			}
		}
		function openAccordion(el) {
			var heads = document.getElementsByClassName("accHead");
			for (var i = 0; i < heads.length; i++) {
				var head = heads[i].childNodes[1];
				head.removeAttribute("style");
				head.childNodes[1].removeAttribute("style");
			}
			var subMenus = document.getElementsByClassName("sub-menu");
			for (var i = 0; i < subMenus.length; i++) {
				subMenus[i].removeAttribute("style");
			}
			el.style.backgroundColor = "#a5cd4e";
			el.style.color = "#1F1F1F";
			el.style.textShadow = "1px 1px 1px rgba(255,255,255, .2)";
			el.childNodes[1].style.backgroundColor = "#3e5706";
			el.childNodes[1].style.color = "#fdfdfd";
			el.childNodes[1].style.textShadow = "0px 1px 0px rgba(0,0,0, .35)";
			var subMenu = el.parentNode.childNodes[3];
			var subItems = subMenu.getElementsByTagName('li');
			subMenuHeight = subItems.length*33;
			subMenu.style.height = subMenuHeight+"px";
		}
		function toggleModal(el, type) {
			if (!modalToggle) {
				showNav(event);
				var modal = document.getElementById('modal');
				var dialog = document.getElementById('dialog');
				var forum = dialog.childNodes[3];
				modal.style.display = 'block';
				dialog.style.opacity = '1';
				dialog.style.visibility = 'visible';
				document.getElementById("resNavDrop").style.maxHeight = "0px";				
				var xmlhttp;
				xmlhttp=new XMLHttpRequest();
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						forum.innerHTML = xmlhttp.responseText;
					}
				}
				if (type == "forum") {
					var catId = el.id.replace("forumCat-","");
					xmlhttp.open("GET","forumIndex.php?i="+catId,true);
					dialog.className = "forumModal";
					document.getElementsByClassName("close")[0].onclick = function() { toggleModal() };
				} else if (type == "about") {
					xmlhttp.open("GET","about.php",true);
					dialog.className = "aboutModal";
					document.getElementsByClassName("close")[0].onclick = function() { toggleModal() };
				} else if (type == "login") {
					xmlhttp.open("GET","login.php",true);
					dialog.className = "loginModal";
					document.getElementsByClassName("close")[0].onclick = function() { toggleModal() };
				} else if (type == "signup") {
					xmlhttp.open("GET","signup.php",true);
					dialog.className = "signupModal";
					document.getElementsByClassName("close")[0].onclick = function() { toggleModal() };
				} else if (type == "logout") {
					xmlhttp.open("GET","logout.php",true);
					dialog.className = "loginModal";
					document.getElementsByClassName("close")[0].onclick = function() { toggleModal() };
				} else if (type == "profile") {
					xmlhttp.open("GET","profile.php",true);
					dialog.className = "profileModal";
					document.getElementsByClassName("close")[0].onclick = function() { toggleModal() };
				} else if (type == "activation") {
					xmlhttp.open("GET","activation.php?ajax",true);
					dialog.className = "ActivationModal";
					document.getElementsByClassName("close")[0].onclick = function() { toggleModal(this, 'activation') };
				} else if (type == "enterComp") {
					xmlhttp.open("GET","enterComp.php",true);
					dialog.className = "compModal";
					document.getElementsByClassName("close")[0].onclick = function() { toggleModal() };
				} else if (type == "voteComp") {
					xmlhttp.open("GET","voteComp.php",true);
					dialog.className = "compModal";
					document.getElementsByClassName("close")[0].onclick = function() { toggleModal() };
				}
				xmlhttp.send();
			} else {
				console.log(type);
				if (type != "activation") {
					var modal = document.getElementById('modal');
					var dialog = document.getElementById('dialog');
					var forum = dialog.childNodes[3];
					modal.style.display = 'none';
					dialog.style.opacity = '0';
					dialog.style.visibility = 'hidden';
					forum.innerHTML = "";
				}
			}
			if (type != "activation") {
				modalToggle = (modalToggle) ? false : true;
			} else {
				modalToggle = true;
			}
		}

		function loadPage(el, page) {
			var fullPage = page+".php";
			var xmlhttp;
			xmlhttp=new XMLHttpRequest();
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					forum.innerHTML = xmlhttp.responseText;
				}
			}
			if (page == "viewTopic") {
				var id = el.className;
				xmlhttp.open("GET",fullPage+"?i="+id,true);
			} else if(page == "newTopic") {
				var id = document.getElementById("catId").textContent;
				xmlhttp.open("GET",fullPage+"?i="+id,true);
			}
			xmlhttp.send();
		}
		function newPost(el, type) {
			if (type == "Topic") {
				var inputSubmit = false;
				var textareaSubmit = false;
				var input = el.childNodes[1];
				var textarea = el.childNodes[3];
				var id = document.getElementById('catId').textContent;
				if (input.value == "") {
					if (input.className.indexOf("invalid") == -1) {
						input.className += " invalid";
					}
				} else {
					input.className = input.className.replace("invalid","");
					inputSubmit = true;
				}
				if (textarea.value == "") {
					if (textarea.className.indexOf("invalid") == -1) {
						textarea.className += " invalid";
					}
				} else {
					textarea.className = textarea.className.replace("invalid","");
					textareaSubmit = true;
				}
				if (inputSubmit && textareaSubmit) {
					var xmlhttp;
					xmlhttp=new XMLHttpRequest();
					xmlhttp.onreadystatechange=function()
					{
						if (xmlhttp.readyState==4 && xmlhttp.status==200)
						{
							if (xmlhttp.responseText.indexOf("Error") != -1) {
								if (xmlhttp.responseText.indexOf("Title") != -1) {
									if (input.className.indexOf("invalid") == -1) {
										input.className += " invalid";
									}
								} else if (xmlhttp.responseText.indexOf("Content") != -1) {
									if (textarea.className.indexOf("invalid") == -1) {
										textarea.className += " invalid";
									}
								}
							} else if(xmlhttp.responseText.indexOf("Success") != -1) {
								var id = xmlhttp.responseText.split("<:::>")[1];
								var xmlhttp1;
								xmlhttp1=new XMLHttpRequest();
								xmlhttp1.onreadystatechange=function()
								{
									if (xmlhttp1.readyState==4 && xmlhttp1.status==200)
									{
										forum.innerHTML = xmlhttp1.responseText;
									}
								}
								xmlhttp1.open("GET","viewTopic.php?i="+id,true);
								xmlhttp1.send();
							}
						}
					}
					xmlhttp.open("POST","addPost.php?a="+type+"&i="+id,true);
					xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded")
					xmlhttp.send("title="+input.value+"&content="+textarea.value);
				}
			} else if (type == "Comment") {
				var inputSubmit = false;
				var textarea = el.childNodes[0];
				var id = document.getElementById("topicId").textContent;
				if (textarea.value == "") {
					if (textarea.className.indexOf("invalid") == -1) {
						textarea.className += " invalid";
					}
				} else {
					textarea.className = textarea.className.replace("invalid","");
					inputSubmit = true;
				}
				if (inputSubmit) {
					var xmlhttp;
					xmlhttp=new XMLHttpRequest();
					xmlhttp.onreadystatechange=function()
					{
						if (xmlhttp.readyState==4 && xmlhttp.status==200)
						{
							if (xmlhttp.responseText.indexOf("Error") != -1) {
								if (xmlhttp.responseText.indexOf("Content") != -1) {
									if (textarea.className.indexOf("invalid") == -1) {
										textarea.className += " invalid";
									}
								}
							} else if(xmlhttp.responseText.indexOf("Success") != -1) {
								var xmlhttp1;
								xmlhttp1=new XMLHttpRequest();
								xmlhttp1.onreadystatechange=function()
								{
									if (xmlhttp1.readyState==4 && xmlhttp1.status==200)
									{
										forum.innerHTML = xmlhttp1.responseText;
									}
								}
								xmlhttp1.open("GET","viewTopic.php?i="+id,true);
								xmlhttp1.send();
							}
						}
					}
					xmlhttp.open("POST","addPost.php?a="+type+"&i="+id,true);
					xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded")
					xmlhttp.send("content="+textarea.value);
				}
			}
		}
		function forumBack(page) {
			var id = document.getElementById("catId").textContent;
			var xmlhttp;
			xmlhttp=new XMLHttpRequest();
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					forum.innerHTML = xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET","forumIndex.php?i="+id,true);
			xmlhttp.send();
		}
		function signUp(el) {
			document.getElementsByClassName("Error")[0].textContent = "";
			var inputs = el.getElementsByTagName("input");
			
			var Username = inputs[0];
			var Password = inputs[1];
			var ConfirmPassword = inputs[2];
			var Email = inputs[3];
			var ConfirmEmail = inputs[4];
			
			var Valid = {Username: true, Password: true, ConfirmPassword: true, Email: true, ConfirmEmail: true};
			
			if (Username.value == "")
				Valid['Username'] = false;
			if (Password.value == "")
				Valid['Password'] = false;
			if (ConfirmPassword.value == "")
				Valid['ConfirmPassword'] = false;
			if (Email.value == "" || !/[A-Z0-9._%+-]+@[A-Z0-9._]+.[A-Z]{2,}/igm.test(Email.value))
				Valid['Email'] = false;
			if (ConfirmEmail.value == "" || !/[A-Z0-9._%+-]+@[A-Z0-9._]+.[A-Z]{2,}/igm.test(ConfirmEmail.value))
				Valid['ConfirmEmail'] = false;
				
			for (var inputs in Valid) {
				var isValid = Valid[inputs];
				document.getElementById(inputs).removeAttribute("style");
				if (!isValid) {
					document.getElementsByClassName("Error")[0].textContent = inputs+" is not valid";
					document.getElementById(inputs).style.borderColor = "#ff0000";
					return false;
				}
			}
			if (Password.value != ConfirmPassword.value) {
				document.getElementsByClassName("Error")[0].textContent = "Supplied passwords do not match";
				document.getElementById("Password").style.borderColor = "#ff0000";
				document.getElementById("ConfirmPassword").style.borderColor = "#ff0000";
				return false;
			}
			if (Email.value != ConfirmEmail.value) {
				document.getElementsByClassName("Error")[0].textContent = "Supplied emails do not match";
				document.getElementById("Email").style.borderColor = "#ff0000";
				document.getElementById("ConfirmEmail").style.borderColor = "#ff0000";
				return false;
			}
			var xmlhttp;
			xmlhttp=new XMLHttpRequest();
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var res = xmlhttp.responseText;
					if (res.indexOf("Registered") != -1) {
						var xmlhttp1;
						xmlhttp1=new XMLHttpRequest();
						xmlhttp1.onreadystatechange=function()
						{
							if (xmlhttp1.readyState==4 && xmlhttp1.status==200)
							{
								location.reload();
							}
						}
						xmlhttp1.open("POST","login.php",true);
						xmlhttp1.setRequestHeader("Content-type","application/x-www-form-urlencoded")
						xmlhttp1.send("Username="+Username.value+"&Password="+Password.value);
					} else if (res.indexOf("Error:") != -1) {
						var resMsg = res.replace("Confirm","Confirm ").replace("Error: ","");
						document.getElementsByClassName("Error")[0].textContent = resMsg;
						var errorEl = res.match(/^Error: (\S*)/)[1];
						document.getElementById(errorEl).style.borderColor = "#ff0000";
					}
				}
			}
			xmlhttp.open("POST","signup.php",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded")
			xmlhttp.send("Username="+Username.value+"&Password="+Password.value+"&ConfirmPassword="+ConfirmPassword.value+"&Email="+Email.value+"&ConfirmEmail="+ConfirmEmail.value);
		}
		function login(el) {
			document.getElementsByClassName("Error")[0].textContent = "";
			
			var inputs = el.getElementsByTagName("input");
			var Username = inputs[0];
			var Password = inputs[1];
			var Valid = {Username: true, Password: true};
			
			if (Username.value == "")
				Valid['Username'] = false;
			if (Password.value == "")
				Valid['Password'] = false;
				
			for (var inputs in Valid) {
				var isValid = Valid[inputs];
				document.getElementById(inputs).removeAttribute("style");
				if (!isValid) {
					document.getElementsByClassName("Error")[0].textContent = inputs+" is not valid";
					document.getElementById(inputs).style.borderColor = "#ff0000";
					return false;
				}
			}
			var xmlhttp;
			xmlhttp=new XMLHttpRequest();
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var res = xmlhttp.responseText;
					if (res.indexOf("loggedin") != -1) {
						location.reload();
					} else if (res.indexOf("Error:") != -1) {
						var resMsg = res.replace("Confirm","Confirm ").replace("Error: ","");
						document.getElementsByClassName("Error")[0].textContent = resMsg;
						var errorEl = res.match(/^Error: (\S*)/)[1];
						document.getElementById(errorEl).style.borderColor = "#ff0000";
					}
				}
			}
			xmlhttp.open("POST","login.php",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded")
			xmlhttp.send("Username="+Username.value+"&Password="+Password.value);
		}
		function logout() {
			var xmlhttp;
			xmlhttp=new XMLHttpRequest();
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var res = xmlhttp.responseText;
					if (res.indexOf("loggedout") != -1) {
						location.reload();
					}
				}
			}
			xmlhttp.open("GET","logout.php?y",true);
			xmlhttp.send();
		}
		function activate(el) {
			el.childNodes[5].textContent = "";
			var activationInput = el.childNodes[7].childNodes[1];
			activationInput.removeAttribute("style");
			if (activationInput.value == "") {
				el.childNodes[5].textContent = "Please specify an activation code.";
				activationInput.style.borderColor = "#ff0000";
			} else {
				var xmlhttp;
				xmlhttp=new XMLHttpRequest();
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						var res = xmlhttp.responseText;
						if (res.indexOf("Activated") != -1) {
							location.reload();
						} else if (res.indexOf("Error:") != -1) {
							var resMsg = res.replace("Error: ","");
							document.getElementsByClassName("Error")[0].textContent = resMsg;
							var errorEl = res.match(/^Error: (\S*)/)[1];
							document.getElementById(errorEl).style.borderColor = "#ff0000";
						}
					}
				}
				xmlhttp.open("POST","activation.php?ajax",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded")
				xmlhttp.send("&b="+activationInput.value);
			}
		}
		function enterComp(el) {
			document.getElementsByClassName("Error")[0].textContent = "";
			console.log(el.childNodes);
			var Repo = el.childNodes[3].childNodes[3];
			var RepoUrl = el.childNodes[5].childNodes[3];
			var Title = el.childNodes[7].childNodes[3];
			var Description = el.childNodes[9].childNodes[3];
			var Valid = {Title: true, RepoUrl: true, Description: true};
			
			if (Title.value == "")
				Valid['Title'] = false;
			if (RepoUrl.value == "")
				Valid['RepoUrl'] = false;
			if (Description.value == "")
				Valid['Description'] = false;
			
			for (var inputs in Valid) {
				var isValid = Valid[inputs];
				document.getElementById(inputs).removeAttribute("style");
				if (!isValid) {
					document.getElementsByClassName("Error")[0].textContent = inputs+" is not valid";
					document.getElementById(inputs).style.borderColor = "#ff0000";
					return false;
				}
			}
			var xmlhttp;
			xmlhttp=new XMLHttpRequest();
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var res = xmlhttp.responseText;
					console.log(res);
					if (res.indexOf("Added") != -1) {
						location.reload();
					} else if (res.indexOf("Error:") != -1) {
						var resMsg = res.replace("Error: ","");
						document.getElementsByClassName("Error")[0].textContent = resMsg;
						var errorEl = res.match(/^Error: (\S*)/)[1];
						document.getElementById(errorEl).style.borderColor = "#ff0000";
					}
				}
			}
			xmlhttp.open("POST","enterComp.php",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded")
			xmlhttp.send("Repo="+Repo.value+"&Title="+Title.value+"&RepoUrl="+RepoUrl.value+"&Description="+Description.value);
		}
		
		function voteComp(el) {
			document.getElementsByClassName("Error")[0].textContent = "";
			console.log(el.childNodes);
			var Entry = el.childNodes[3].childNodes[3];
			var Valid = {Entry: true};
			
			if (Entry.value == "")
				Valid['Entry'] = false;
			
			for (var inputs in Valid) {
				var isValid = Valid[inputs];
				document.getElementById(inputs).removeAttribute("style");
				if (!isValid) {
					document.getElementsByClassName("Error")[0].textContent = inputs+" is not valid";
					document.getElementById(inputs).style.borderColor = "#ff0000";
					return false;
				}
			}
			var xmlhttp;
			xmlhttp=new XMLHttpRequest();
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					var res = xmlhttp.responseText;
					console.log(res);
					if (res.indexOf("Added") != -1) {
						location.reload();
					} else if (res.indexOf("Error:") != -1) {
						var resMsg = res.replace("Error: ","");
						document.getElementsByClassName("Error")[0].textContent = resMsg;
						var errorEl = res.match(/^Error: (\S*)/)[1];
						document.getElementById(errorEl).style.borderColor = "#ff0000";
					}
				}
			}
			xmlhttp.open("POST","voteComp.php",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded")
			xmlhttp.send("Entry="+Entry.value);
		}
		var postContent = "";
		function enableEditPost(el, type) {
			curPost = el.parentNode.parentNode.childNodes[4];
			postContent = curPost.textContent;
			curPost.innerHTML = "<textarea onblur=\"updatePost(this, '"+type+"')\">"+postContent+"</textarea>";
			curPost.childNodes[0].focus();
		}
		function updatePost(el, type) {
			var editID = el.parentNode.parentNode.childNodes[2].textContent;
			el.parentNode.innerHTML = "<span>"+nl2br(el.value)+"</span>";
			if (postContent != el.value) {
				var xmlhttp;
				xmlhttp=new XMLHttpRequest();
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						var res = xmlhttp.responseText;
						if (res.indexOf("Success") == -1) {
							alert("Unable to update ;-( Please don't hurt me!");
						}
					}
				}
				xmlhttp.open("POST","updatePost.php",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded")
				xmlhttp.send("a="+type+"&content="+el.value+"&id="+editID);
			}
		}
		function deletePost(el, type) {
			var delID = el.parentNode.parentNode.childNodes[2].textContent;
			var topicId = document.getElementById("topicId").textContent;
			var catId = document.getElementById("catId").textContent;
			if (postContent != el.value) {
				var xmlhttp;
				xmlhttp=new XMLHttpRequest();
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						var res = xmlhttp.responseText;
						if (res.indexOf("Success") == -1) {
							alert("Unable to delete ;-( Please don't hurt me!");
						} else {
								var xmlhttp1;
								xmlhttp1=new XMLHttpRequest();
								xmlhttp1.onreadystatechange=function()
								{
									if (xmlhttp1.readyState==4 && xmlhttp1.status==200)
									{
										forum.innerHTML = xmlhttp1.responseText;
									}
								}
								if (type == "post") {
									xmlhttp1.open("GET","viewTopic.php?i="+topicId,true);
								} else {
									xmlhttp1.open("GET","forumIndex.php?i="+catId,true);
								}
								xmlhttp1.send();
						}
					}
				}
				xmlhttp.open("POST","deletePost.php",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded")
				xmlhttp.send("a="+type+"&id="+delID);
			}
		}
		function nl2br(str, is_xhtml) {
			var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
			return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
		}