<?php
	if (isset($_POST) && !empty($_POST)) {
		include 'includes/functions.inc.php';
		include 'includes/db.inc.php';
		foreach($_POST AS $key => $value) {
			if (empty($value)) {
				die("Error: ".$key." is not valid");
			}
		}
		$password = pass($_POST['Password']);
		try { 
			$result = $pdo->prepare("SELECT ID FROM users WHERE Name = :Name");
			$result->bindParam(":Name", $_POST['Username']);
			$result->execute();
			$numRows = $result->rowCount();
		} catch (PDOException $e) {
			exception($result->errorInfo(), $e);
			die("Error: Unable to fetch users");
		}
		if ($numRows > 0)
			die("Error: Username is already in use");
			
		try {
			$result = $pdo->prepare("SELECT ID FROM users WHERE Email = :Email");
			$result->bindParam(":Email", $_POST['Email']);
			$result->execute();
			$numRows = $result->rowCount();
		} catch (PDOException $e) {
			exception($result->errorInfo(), $e);
			die("Error: Unable to fetch emails");
		}
		if ($numRows > 0)
			die("Error: Email is already in use");
			
		try {
			$result = $pdo->prepare("INSERT INTO users (Name, Password, Email, DisplayName, Code) VALUES (:Name, :Password, :Email, :Name, MD5(:Email))");
			$result->bindParam(":Name", $_POST['Username']);
			$result->bindParam(":Password", $password);
			$result->bindParam(":Email", $_POST['Email']);
			$result->execute();
			
			$from = "noreply@".str_replace('www.', '', $_SERVER['HTTP_HOST']);
			$to = $_POST['Email'];
			$subject = 'Node.js Academy Email Confirmation';
			$headers = "From: " . $from . "\r\n";
			$headers .= "Reply-To: ". $from . "\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			
			$message = '<html><body>';
			$message .= '<h1>Hello '.$_POST['Username'].'.</h1>';
			$message .= '<h2>Welcome to Node.js Academy!</h2>';
			$message .= '<p>Please confirm your email by clicking <a href="http://'.$_SERVER['HTTP_HOST'].'activate.php?a='.$_POST['Email'].'&b='.MD5($_POST['Email']).'">here</a>.<br/><br/>';
			$message .= 'If that link doesn\'t work, here is the activation code.<br/>';
			$message .= MD5($_POST['Email']).'<br/>';
			$message .= 'This code can be pasted into the field that should appear when you next visit our website.</p>';
			$message .= '</body></html>';
			
			mail($to, $subject, $message, $headers);
			
			echo "Registered";
		} catch (PDOException $e) {
			exception($result->errorInfo(), $e);
			die("Error: Unable to register user");
		}
	} else {
		include 'signup.html.php';
	}
?>