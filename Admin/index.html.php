		<section id="section">
			<div id="stats">
				<h3 class="top">Dashboard</h3>
				<div class="bottom">
					<div class="content">
					<p class="sub">General Stats</p>
						<ul>
							<li>
								<span class="row_title"><?php echo $counts['Topics'] ?></span>
								<span class="row_data">Topics</span>
							</li>
							<li>
								<span class="row_title"><?php echo $counts['Comments'] ?></span>
								<span class="row_data">Comments</span>
							</li>
							<li>
								<span class="row_title"><?php echo $counts['Users'] ?></span>
								<span class="row_data">Users</span>
							</li>
						</ul>
					</div>
					<div class="activity">
						<p class="sub">Recent Activity</p>
						<ul>
						<?php foreach($recentPosts AS $activity) { ?>
							<li>
							<?php if (end($activity) == 'topic') { ?>
								<span class="row_title"><?php echo timeago(strtotime($activity['Date'])); ?></span>
								<span class="row_data"><?php echo $activity['Actor']; ?> Posted topic <b><?php echo $activity['Subject'] ?></b> in <b><?php echo $activity['Parent'] ?></b></span>
							<?php } elseif (end($activity) == 'comment') { ?>
								<span class="row_title"><?php echo timeago(strtotime($activity['Date'])); ?></span>
								<span class="row_data"><?php echo $activity['Actor']; ?> Posted comment <b>Re: <?php echo $activity['Subject'] ?></b> in <b><?php echo $activity['Parent'] ?></b></span>
							<?php } ?>
							</li>
						<?php } ?>
						</ul>
					</div>
					<div class="notes">
					<p class="sub">Admin Notes</p>
					<textarea id="notes" placeholder="Put notes for other Admins here" onchange="updateNotes(this.value)"><?php echo $notes; ?></textarea>
					</div>
				</div>
			</div>
		</section>
	</body>
	<script type="text/javascript">
	function updateNotes(text) {
		var xmlhttp;
		if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else { // code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function () {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				if (xmlhttp.responseText != "Added") {
					document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Error updating admin notes</a>";
				} else {
					document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Admin notes updated</a>";
				}
				var loc = window.location.href,
				index = loc.indexOf('#');
				//console.log(index);
				if (index > 0) {
					window.location.replace("#");
					if (typeof window.history.replaceState == 'function') {
						history.replaceState({},'',window.location.href.slice(0,-1));
					}
				//console.log("clear hash");
				}
			}
		}
		xmlhttp.open("POST", "updateNotes.php", true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("Text="+text);
	}
	</script>
</html>