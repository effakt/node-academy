		<section id="section">
			<div id="stats">
				<h3 class="top">Lessons</h3>
				<div class="bottom">
					<div class="content">
						<p class="sub">All Lessons</p>
						<ul id="boards">
						<?php foreach ($categories AS $category) { ?>
							<li class="category">
								<span class='title'><?php echo $category['Name'] ?></span>
							</li>
							<?php foreach($lessons AS $lesson) { ?>
								<?php if ($lesson['Category'] == $category['ID']) { ?>
									<li class='<?php echo $lesson['ID']; ?>' id='board<?php echo $lesson['ID'] ?>'>
									
										<span onclick="enableEditLesson(this.parentNode)" style="font-size: 0.8em; float: right; cursor: pointer; margin-right: 5px;">Edit</span>
										<span class='title'><?php echo $lesson['Name']; ?></span>
										<p class='content' hidden><?php echo htmlentities($lesson['Content']) ?></p>
										<p class='categoryid' hidden><?php echo $lesson['Category'] ?></p>
										<p class='category' hidden><?php echo $category['Name'] ?></p>
										<p class='type' hidden><?php echo $lesson['Type'] ?></p>
										<p class='answer' hidden><?php echo $lesson['Answer'] ?></p>
										<p class='id' hidden><?php echo $lesson['ID'] ?></p>
									</li>
								<?php } ?>
							<?php } ?>
						<?php } ?>
						</ul><br/>
					</div>
					<div class="content">
					<p class="sub">Add Lessons</p>
						<form onsubmit="return addLesson()" id="addForm">
							<input type="hidden" id="Category" value="<?php echo end($categories)['ID']; ?>">
							<input type="text" disabled value="<?php echo end($categories)['Name']; ?>">
								<br/>
							<input type="text" id="Name" placeholder="Name" required/><br/>
							<textarea id="Desc" placeholder="Content" required style="font-size: 1em;"></textarea><br/>
							<select style="margin-top: 5px;" id="Type" onchange="toggleAnswer();">
								<option value="text">Lesson</option>
								<option value="code">Challenge</option>
							</select>
							<input type="text" name="answer" id="Answer" style="display: none;" placeholder="Answer" />
							<input type="submit" value="Add Lesson"/><input type="reset" value="Reset Form" onclick="resetForm();"/>
						</form>
					</div>
				</div>
			</div>
		</section>
<script>
function toggleAnswer() {
var answer = document.getElementById("Answer");
	if (document.getElementById("Type").value == "text") {
		answer.style.display = "none";
		answer.removeAttribute("required");
	} else {
		answer.style.display = "block";
		answer.setAttribute("required", "true");
	}
}
function enableEditLesson(el) {
	var form = document.getElementById('addForm').childNodes;
	var lessonInfo = el.childNodes;
	form[1].value = lessonInfo[7].textContent;
	form[3].value = lessonInfo[9].textContent;
	form[7].value = lessonInfo[3].textContent;
	form[10].value = lessonInfo[5].textContent;
	form[13].value = lessonInfo[11].textContent.toLowerCase();
	toggleAnswer();
	form[15].value = lessonInfo[13].textContent;
	form[17].value = "Edit Lesson";
	document.getElementById('addForm').onsubmit = function() { return editLesson(lessonInfo[15].textContent) };
	document.getElementById('addForm').previousSibling.previousSibling.textContent = "Edit Lesson";
}
function editLesson(id) {
	var cat = document.getElementById("Category");
	var name = document.getElementById("Name");
	var desc = document.getElementById("Desc");
	var type = document.getElementById("Type");
	var answer = document.getElementById("Answer");
	var xmlhttp;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else { // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var res = xmlhttp.responseText;
			if (res == "Updated") {
				location.reload();
			} else {
				document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Error updating lesson</a>";
				var loc = window.location.href,
				index = loc.indexOf('#');
				if (index > 0) {
					window.location.replace("#");
					if (typeof window.history.replaceState == 'function') {
						history.replaceState({},'',window.location.href.slice(0,-1));
					}
				}
			}
		}
	}
	xmlhttp.open("POST", "editLesson.php", true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	if (type.value == "text")
		xmlhttp.send("Name="+name.value+"&Desc="+desc.value+"&Category="+cat.value+"&Type="+type.value+"&ID="+id);
	else
		xmlhttp.send("Name="+name.value+"&Desc="+desc.value+"&Category="+cat.value+"&Type="+type.value+"&Answer="+answer.value+"&ID="+id);
	return false;
}
function resetForm(el) {
	var form = document.getElementById('addForm').childNodes;
	form[17].value="Add Lesson";
	document.getElementById('addForm').onsubmit = function() { return addLesson() };
	document.getElementById('addForm').previousSibling.previousSibling.textContent = "Add Lesson";
}
function addLesson() {
	var cat = document.getElementById("Category");
	var name = document.getElementById("Name");
	var desc = document.getElementById("Desc");
	var type = document.getElementById("Type");
	var answer = document.getElementById("Answer");
	
	//run ajax to update db
	var xmlhttp;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else { // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var res = xmlhttp.responseText;
			if (res == "Added") {
				location.reload();
			} else {
				document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Error adding lesson</a>";
				var loc = window.location.href,
				index = loc.indexOf('#');
				if (index > 0) {
					window.location.replace("#");
					if (typeof window.history.replaceState == 'function') {
						history.replaceState({},'',window.location.href.slice(0,-1));
					}
				}
			}
		}
	}
	xmlhttp.open("POST", "addLesson.php", true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	if (type.value == "text")
		xmlhttp.send("Name="+name.value+"&Desc="+desc.value+"&Category="+cat.value+"&Type="+type.value);
	else
		xmlhttp.send("Name="+name.value+"&Desc="+desc.value+"&Category="+cat.value+"&Type="+type.value+"&Answer="+answer.value);
	return false;
}
</script>
	</body>
</html>