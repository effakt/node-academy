		<style>
		table {
			border-collapse: collapse;
			border-spacing: 0;
			empty-cells: show;
			border: 1px solid #cbcbcb;
			table-layout: fixed;
			width: 108%;
			margin-top:5px;
		}
		thead {
			background: #e0e0e0;
			color: #000;
			text-align: left;
			vertical-align: bottom;
		}
		td:first-child, th:first-child {
			border-left-width: 0;
		}
		th, td {
			padding: 0.5em 0.5em;
			overflow: hidden;
			word-wrap: break-word;
		}
		tr {
			overflow: hidden;
		}
		tr:not(:last-child) {
			border-bottom: 1px solid #cbcbcb;
		}
		td:nth-child(4){
			text-align: center;
		}
		th {
			text-align: center;
		}
		span.clearErrors {
			float: right;
			cursor: pointer;
		}
		span.clearErrors:hover {
			text-decoration: underline;
		}
		span.clearErrors a {
			color: #000;
			text-decoration: none;
		}
		</style>
		<section id="section">
			<div id="stats">
				<h3 class="top">Errors</h3>
				<div class="bottom">
					<div class="content">
					<p class="sub">All Errors</p>
						<span class="clearErrors"><a href="?del">Clear all errors</a></span>
						<table>
							<thead>
								<tr>
									<th width="8%">State</th>
									<th width="8%">Code</th>
									<th>Message</th>
									<th width="8%">Line</th>
									<th>File</th>
									<th width="10%">Date</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($errors AS $error) { ?>
									<tr>
										<td><?php echo $error['State'] ?></td>
										<td><?php echo $error['Code'] ?></td>
										<td><?php echo $error['Message'] ?></td>
										<td><?php echo $error['Line'] ?></td>
										<td><?php echo $error['File'] ?></td>
										<td><?php echo date("d M Y", strtotime($error['Date'])) ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
	</body>
</html>