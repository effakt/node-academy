		<section id="section">
			<div id="stats">
				<h3 class="top">Users</h3>
				<div class="bottom">
					<div class="content">
					<p class="sub">All Users</p>
						<ul id="users">
						<li class='Head' id='cat'>
								<span class='name'>Display Name</span>
								<span class='email'>Email</span>
								<span class='role'>Role</span>
								<span class='progress'>Progress</span>
								<span class='banned'>Banned</span>
							</li>
						<?php foreach($users AS $user) { ?>
							<li class='' id='cat'>
								<span class='name'><?php echo $user['DisplayName']; ?></span>
								<span class='email'><?php echo $user['Email']; ?></span>
								<span class='role'><?php echo $user['Role']; ?></span>
								<span class='progress'><?php echo $user['Progress']; ?></span>
								<span class='banned'><label><input type="checkbox" name="banned" value="<?php echo $user['ID']; ?>" <?php if ($user['Banned']) echo "checked"; if ($user['Role'] == "Admin") echo "disabled" ?> onchange="banAccount(this)" class="ban" /><div class="switch"></div></label></span>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<script>
			function banAccount(el) {
				var xmlhttp;
				if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				} else { // code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						if (xmlhttp.responseText == "Banned") {
							document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Successfully banned user</a>";
							var loc = window.location.href,
							index = loc.indexOf('#');
							if (index > 0) {
								window.location.replace("#");
								if (typeof window.history.replaceState == 'function') {
									history.replaceState({},'',window.location.href.slice(0,-1));
								}
							}
						} else if (xmlhttp.responseText == "Unbanned") {
							document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Successfully unbanned user</a>";
							var loc = window.location.href,
							index = loc.indexOf('#');
							if (index > 0) {
								window.location.replace("#");
								if (typeof window.history.replaceState == 'function') {
									history.replaceState({},'',window.location.href.slice(0,-1));
								}
							}
						} else {
							document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Error banning/unbanning user</a>";
							var loc = window.location.href,
							index = loc.indexOf('#');
							if (index > 0) {
								window.location.replace("#");
								if (typeof window.history.replaceState == 'function') {
									history.replaceState({},'',window.location.href.slice(0,-1));
								}
							}
						}
					}
				}
				xmlhttp.open("POST", "banAccount.php", true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send("a="+el.value+"&b="+ +el.checked);
			}
		</script>
	</body>
</html>