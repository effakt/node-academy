		<section id="section">
			<div id="stats">
				<h3 class="top">Todo</h3>
				<div class="bottom">
					<div class="content">
					<p class="sub">Todo List</p>
						<div class="todo">
							<ul class="todo-controls">
								<li><a href="javascript:addTodoEl()" class="icon-plus"></a></li>
								<li><a href="javascript:deleteTodo();" class="icon-trash"></a></li>
							</ul>
						
							<ul class="todo-list">
							<?php if (count($todos) > 0) { ?>
								<?php foreach ($todos AS $todo) { ?>
									<li><input type="checkbox" class="todoCheckbox" <?php if ($todo['Checked'] == 1) { echo "checked"; } ?> id="item<?php echo $todo['ID']; ?>" onchange="updateTodo(this);"/> <label class="toggle" for="item<?php echo $todo['ID']; ?>"></label><?php echo $todo['Value']; ?></li>
								<?php } ?>
							<?php } else { ?>
								<li><input type="checkbox" class="todoCheckbox" disabled /> <label class="toggle"></label>There aren't any todo! maybe add some?</li>
							<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
		<script>
			function addTodoEl() {
				var todo = document.getElementsByClassName("todo")[0].childNodes[3];
				var todos = todo.getElementsByTagName("li");
				var noTodos = todos.length;
				var lastTodo = todos[noTodos-1];
				var lastTodoId = lastTodo.childNodes[0].id;
				var lastTodoDbId = lastTodoId.replace("item","");
				todo.innerHTML += "<li><input type='checkbox' class='todoCheckBox' id='item"+(+lastTodoDbId+1)+"' disabled/><label class='toggle' for='item"+(+lastTodoDbId+1)+"'></label><input type='text' class='input"+(+lastTodoDbId+1)+"' onchange='addTodo(this.value);'/></li>";
				document.getElementsByClassName("input"+(+lastTodoDbId+1))[0].focus();
			}
			function addTodo(a) {
				var xmlhttp;
				if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				} else { // code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						if (xmlhttp.responseText.indexOf("Added") != -1) {
							document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Successfully added todo</a>";
							var loc = window.location.href,
							index = loc.indexOf('#');
							if (index > 0) {
								window.location.replace("#");
								if (typeof window.history.replaceState == 'function') {
									history.replaceState({},'',window.location.href.slice(0,-1));
								}
							}
							
							var todoList = JSON.parse(xmlhttp.responseText.split("<:::>")[1]);
							var todo = document.getElementsByClassName("todo")[0].childNodes[3];
							todo.innerHTML = "";
							for (var i = 0; i < todoList.length; i++) {
								if (todoList[i]['Checked'] == 0)
									todo.innerHTML += "<li><input type='checkbox' class='todoCheckbox' id='item"+todoList[i]['ID']+"' onchange='updateTodo(this);'><label class='toggle' for='item"+todoList[i]['ID']+"'></label>"+todoList[i]['Value']+"</li>";
								else
									todo.innerHTML += "<li><input type='checkbox' class='todoCheckbox' id='item"+todoList[i]['ID']+"' onchange='updateTodo(this);' checked><label class='toggle' for='item"+todoList[i]['ID']+"'></label>"+todoList[i]['Value']+"</li>";
							}
							
						} else {
							document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Error adding todo</a>";
							var loc = window.location.href,
							index = loc.indexOf('#');
							if (index > 0) {
								window.location.replace("#");
								if (typeof window.history.replaceState == 'function') {
									history.replaceState({},'',window.location.href.slice(0,-1));
								}
							}
						}
					}
				}
				xmlhttp.open("POST", "updateTodo.php", true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send("a=a&b="+a);
			}
			function deleteTodo() {
				if (getCheckedBoxes() == 0) {
					document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Please select a todo to delete</a>";
					var loc = window.location.href,
					index = loc.indexOf('#');
					if (index > 0) {
						window.location.replace("#");
						if (typeof window.history.replaceState == 'function') {
							history.replaceState({},'',window.location.href.slice(0,-1));
						}
					}
				} else {
					var p = confirm("Are you sure you want to delete all checked todo?");
					var delTodo = [];
					if (p) {
						var checkedBoxes = getCheckedBoxes();
						for (var i = 0; i < checkedBoxes.length; i++) {
							var checkedBoxId = checkedBoxes[i].id.replace("item","");
							delTodo.push(checkedBoxId);
						}
						var delTodoJson = JSON.stringify(delTodo);
					}
					
					var xmlhttp;
					if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
						xmlhttp = new XMLHttpRequest();
					} else { // code for IE6, IE5
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					xmlhttp.onreadystatechange=function()
					{
						if (xmlhttp.readyState==4 && xmlhttp.status==200)
						{
							if (xmlhttp.responseText == "Deleted") {
								var allCheckboxes = document.querySelectorAll('input[type=checkbox]');
								
								if (allCheckboxes.length == getCheckedBoxes().length) {
									var todo = document.getElementsByClassName("todo")[0].childNodes[3];
									todo.innerHTML = "<li><input type='checkbox' class='todoCheckbox' disabled /> <label class='toggle'></label>There aren't any todo! maybe add some?</li>";
								}
								for (var i = 0; i < checkedBoxes.length; i++) {
									checkedBoxes[i].parentNode.remove();
								}
							
								document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Successfully deleted checked todo</a>";
								var loc = window.location.href,
								index = loc.indexOf('#');
								if (index > 0) {
									window.location.replace("#");
									if (typeof window.history.replaceState == 'function') {
										history.replaceState({},'',window.location.href.slice(0,-1));
									}
								}
							} else {
								document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Error deleting checked todo</a>";
								var loc = window.location.href,
								index = loc.indexOf('#');
								if (index > 0) {
									window.location.replace("#");
									if (typeof window.history.replaceState == 'function') {
										history.replaceState({},'',window.location.href.slice(0,-1));
									}
								}
							}
						}
					}
					xmlhttp.open("POST", "updateTodo.php", true);
					xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
					xmlhttp.send("a=d&b="+delTodoJson);
				}
			}
			
			function updateTodo(a) {
				var id = a.id.replace("item","");
				var checked = +a.checked;
				var xmlhttp;
				if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				} else { // code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						if (xmlhttp.responseText.indexOf("Updated") != -1) {
							document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Successfully updated todo</a>";
							var loc = window.location.href,
							index = loc.indexOf('#');
							if (index > 0) {
								window.location.replace("#");
								if (typeof window.history.replaceState == 'function') {
									history.replaceState({},'',window.location.href.slice(0,-1));
								}
							}
						} else {
							document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Error updating todo</a>";
							var loc = window.location.href,
							index = loc.indexOf('#');
							if (index > 0) {
								window.location.replace("#");
								if (typeof window.history.replaceState == 'function') {
									history.replaceState({},'',window.location.href.slice(0,-1));
								}
							}
						}
					}
				}
				xmlhttp.open("POST", "updateTodo.php", true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send("a=u&b="+id+"&c="+checked);
			}
			
			function getCheckedBoxes() {
				var checkboxes = document.querySelectorAll('input[type=checkbox]');
				var checkboxesChecked = [];
				for (var i=0; i<checkboxes.length; i++) {
					if (checkboxes[i].checked) {
						checkboxesChecked.push(checkboxes[i]);
					}
				}
				return checkboxesChecked.length > 0 ? checkboxesChecked : 0;
			}
		</script>
	</body>
</html>