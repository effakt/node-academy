<?php
	include 'header.php';		
	if (isset($_SESSION['u']) && !empty($_SESSION['u'])) {
		if ($userRole == "Admin") {
			//get competitions
			try {
				$result = $pdo->query("SELECT * FROM competitions");
			} catch (PDOException $e) {
			
			exception($result->errorInfo(), $e);
				die('Error fetching competitions from the database!');
			}
			$competitions = $result->fetchAll();
			include 'nodejam.html.php';
		} else {
			echo "Access Denied";
		}
	} else {
		header("location: ./");
	}
?>