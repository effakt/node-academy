<?php
	include 'header.php';		
	if (isset($_SESSION['u']) && !empty($_SESSION['u'])) {
		if ($userRole == "Admin") {
			//get todos
			try {
				$result = $pdo->query("SELECT * FROM todo");
			} catch (PDOException $e) {
			exception($result->errorInfo(), $e);
				die('Error fetching todo from the database!');
			}
			$todos = $result->fetchAll();
			include 'todo.html.php';
		} else {
			echo "Access Denied";
		}
	} else {
		header("location: ./");
	}
?>