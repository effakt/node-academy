<?php
	include '../includes/access.inc.php';
	include '../includes/functions.inc.php';
	include '../includes/db.inc.php';
	foreach($_POST AS $key => $value) {
		if (empty($value)) {
			die("Error: ".$key." is not valid");
		}
	}
	//var_export($_POST);
	$_POST['Date'] = $_POST['Date']." ".$_POST['Time'].":00";
	var_export($_POST);
	try {
		$result = $pdo->prepare("SELECT Date FROM competitions WHERE Date BETWEEN DATE_SUB(NOW(), INTERVAL 96 HOUR) AND :date");
		$result->bindParam(":date", $_POST['Date']);
		$result->execute();
	} catch (PDOException $e) {
		exception($result->errorInfo(), $e);
		die("Error: Unable to get running competitions");
	}
	echo $result->rowCount();
	if ($result->rowCount() == 0) {
		try {
			$result = $pdo->prepare("INSERT INTO competitions (Subject, Description, Date) VALUES (:subject, :description, :date)");
			$result->bindParam(":subject", $_POST['Name']);
			$result->bindParam(":description", $_POST['Desc']);
			$result->bindParam(":date", $_POST['Date']);
			//$result->execute();
			echo "Added<:::>";
		} catch (PDOException $e) {
			exception($result->errorInfo(), $e);
			die("Error: Unable to add competition");
		}
	} else {
		die("Error: Competition will be running at this time");
	}
?>