<?php
	include '../includes/access.inc.php';
	include '../includes/functions.inc.php';
	include '../includes/db.inc.php';
	foreach($_POST AS $key => $value) {
		if (empty($value)) {
			die("Error: ".$key." is not valid");
		}
	}
	try {
		if ($_POST['Type'] == "text") {
			$result = $pdo->prepare("INSERT INTO lessons (Name, Content, Type, Category) VALUES (:name, :content, :type, :category)");
			$result->bindParam(":name",$_POST['Name']);
			$result->bindParam(":content",$_POST['Desc']);
			$result->bindParam(":type",$_POST['Type']);
			$result->bindParam(":category",$_POST['Category']);
			$result->execute();
		} else {
			$result = $pdo->prepare("INSERT INTO lessons (Name, Content, Type, Answer, Category) VALUES (:name, :content, :type, :answer, :category)");
			$result->bindParam(":name",$_POST['Name']);
			$result->bindParam(":content",$_POST['Desc']);
			$result->bindParam(":type",$_POST['Type']);
			$result->bindParam(":category",$_POST['Category']);
			$result->bindParam(":answer",$_POST['Answer']);
			$result->execute();
		}
		$result = $pdo->exec("UPDATE users SET Complete = 0, Progress = Progress+1 WHERE Complete = 1");
		echo "Added";
	} catch (PDOException $e) {
		exception($result->errorInfo(), $e);
		die("Error: Unable to add lesson");
	}
?>