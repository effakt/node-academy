<?php
	include '../includes/access.inc.php';
	include '../includes/functions.inc.php';
	include '../includes/db.inc.php';
	foreach($_POST AS $key => $value) {
		if (empty($value)) {
			die("Error: ".$key." is not valid");
		}
	}
	try {
		if ($_POST['Type'] == "text") {
			$result = $pdo->prepare("UPDATE lessons SET Name = :name, Content = :content, Type = :type, Category = :category WHERE ID = :id");
			$result->bindParam(":name",$_POST['Name']);
			$result->bindParam(":content",$_POST['Desc']);
			$result->bindParam(":type",$_POST['Type']);
			$result->bindParam(":category",$_POST['Category']);
			$result->bindParam(":id",$_POST['ID']);
			$result->execute();
		} else {
			$result = $pdo->prepare("UPDATE lessons SET Name = :name, Content = :content, Type = :type, Category = :category, Answer = :answer WHERE ID = :id");
			$result->bindParam(":name",$_POST['Name']);
			$result->bindParam(":content",$_POST['Desc']);
			$result->bindParam(":type",$_POST['Type']);
			$result->bindParam(":category",$_POST['Category']);
			$result->bindParam(":answer",$_POST['Answer']);
			$result->bindParam(":id",$_POST['ID']);
			$result->execute();
		}
		echo "Updated";
	} catch (PDOException $e) {
		exception($result->errorInfo(), $e);
		die("Error: Unable to update lesson");
	}
?>