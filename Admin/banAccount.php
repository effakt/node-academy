<?php
	include '../includes/functions.inc.php';
	include '../includes/db.inc.php';
	include '../includes/access.inc.php';
	if ($userRole == "Admin") {
		$user = $_POST['a'];
		$value = $_POST['b'];
		try {
			$result = $pdo->prepare("UPDATE users SET Banned = :val WHERE ID = :user");
			$result->bindParam(':val', $value);
			$result->bindParam(':user', $user);
			$result->execute();
			if ($value)
				echo "Banned";
			else
				echo "Unbanned";
		} catch (PDOException $e) {
			exception($result->errorInfo(), $e);
			die("Error: Unable to ban user");
		}
	}
?>