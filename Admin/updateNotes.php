<?php
	include '../includes/access.inc.php';
	include '../includes/functions.inc.php';
	include '../includes/db.inc.php';
	try {
		$result = $pdo->prepare("UPDATE settings SET Value = :text WHERE Name = 'Notes'");
		$result->bindParam(":text", $_POST['Text']);
		$result->execute();
		echo "Added";
	} catch (PDOException $e) {
		exception($result->errorInfo(), $e);
		die("Error: Unable to update notes");
	}
?>