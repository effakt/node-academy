		<section id="section">
			<div id="stats">
				<h3 class="top">Category</h3>
				<div class="bottom">
					<div class="content">
					<p class="sub">All Category</p>
						<ul id="categories">
						<?php foreach($categories AS $category) { ?>
							<li class='<?php echo $category['ID']; ?>' id='cat<?php echo $category['ID'] ?>'>
								<span class='title'><?php echo $category['Name']; ?></span>
							</li>
							<?php } ?>
						</ul>
						</div>
					<div class="content">
					<p class="sub">Add Category</p>
						<form onsubmit="return addCat()" id="addForm">
							<input type="text" id="Name" placeholder="Name" required/><br/>
							<input type="submit" value="Add Category"/>
						</form>
					</div>
				</div>
			</div>
		</section>
<script>
function addCat() {
	var name = document.getElementById("Name");
	
	//run ajax to update db
	var xmlhttp;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else { // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var res = xmlhttp.responseText.split("<:::>")[0];
			if (res == "Added") {
				var rows = JSON.parse(xmlhttp.responseText.split("<:::>")[1]);
				//remove children
				var categories = document.getElementById("categories");
				while (categories.firstChild) {
					categories.removeChild(categories.firstChild);
				}
				
				//re-add with new
				for (var i = 0; i < rows.length; i++) {
					var newCat = document.createElement("li");
					newCat.setAttribute('class', rows[i]['ID']);
					newCat.setAttribute('id', 'cat'+rows[i]['ID']);
					newCat.setAttribute('ondragstart', 'drag(event)');
					newCat.setAttribute('draggable', 'true');
					
					var newTitle = document.createElement("span");
					newTitle.setAttribute('class','title');
					newTitle.appendChild(document.createTextNode(rows[i]['Name']));
					newCat.appendChild(newTitle);
					
					categories.appendChild(newCat);
				}
				document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Successfully added Category</a>";
				var loc = window.location.href,
				index = loc.indexOf('#');
				if (index > 0) {
					window.location.replace("#");
					if (typeof window.history.replaceState == 'function') {
						history.replaceState({},'',window.location.href.slice(0,-1));
					}
				}
			} else {
				document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Error adding Category</a>";
				var loc = window.location.href,
				index = loc.indexOf('#');
				if (index > 0) {
					window.location.replace("#");
					if (typeof window.history.replaceState == 'function') {
						history.replaceState({},'',window.location.href.slice(0,-1));
					}
				}
			}
		}
	}
	xmlhttp.open("POST", "addCategory.php", true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("Name="+name.value);
	return false;
}
</script>
	</body>
</html>