		<section id="section">
			<div id="stats">
				<h3 class="top">Competitions</h3>
				<div class="bottom">
					<div class="content">
					<p class="sub">All Competitions</p>
						<ul id="categories">
						<?php foreach($competitions AS $competition) { ?>
							<li class='<?php echo $competition['ID']; ?>' id='cat<?php echo $competition['ID'] ?>' ondragstart='drag(event)' draggable='true'>
								<span class='title'><?php echo $competition['Subject']; ?></span>
								<span class='desc'><?php echo $competition['Description']; ?></span>
								<span class='datetime'><?php echo $competition['Date']; ?></span>
							</li>
							<?php } ?>
						</ul>
						<span id='delete' ondragover="dragOver(event)" ondrop="drop(event)" ondragleave="dragLeave(event)">Drag Competitions here to delete</span>
					</div>
					<div class="content">
					<p class="sub">Add Competitions</p>
						<form onsubmit="return addCompetition()" id="addForm">
							<input type="text" id="Name" placeholder="Event name" required/><br/>
							<textarea id="Desc" placeholder="Event description" required></textarea><br/>
							<input type="date" id="Date" placeholder="YYYY-MM-DD" required/><br/>
							<input type="time" id="Time" placeholder="HH:MM" required/><br/>
							Timezone: <?php echo date('e (T)'); ?><br/>
							<input type="submit" value="Add Event"/>
						</form>
					</div>
				</div>
			</div>
		</section>
<script>

function addCompetition() {
	var name = document.getElementById("Name");
	var desc = document.getElementById("Desc");
	var date = document.getElementById("Date");
	var time = document.getElementById("Time");
	//run ajax to update db
	var xmlhttp;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else { // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var res = xmlhttp.responseText.split("<:::>")[0];
			if (res == "Added") {
				var rows = JSON.parse(xmlhttp.responseText.split("<:::>")[1]);
				//remove children
				var categories = document.getElementById("categories");
				while (categories.firstChild) {
					categories.removeChild(categories.firstChild);
				}
				
				//re-add with new
				for (var i = 0; i < rows.length; i++) {
					var newCat = document.createElement("li");
					newCat.setAttribute('class', rows[i]['ID']);
					newCat.setAttribute('id', 'cat'+rows[i]['ID']);
					newCat.setAttribute('ondragstart', 'drag(event)');
					newCat.setAttribute('draggable', 'true');
					
					var newTitle = document.createElement("span");
					newTitle.setAttribute('class','title');
					newTitle.appendChild(document.createTextNode(rows[i]['Name']));
					newCat.appendChild(newTitle);
					
					var newDesc = document.createElement("span");
					newDesc.setAttribute('class','desc');
					newDesc.appendChild(document.createTextNode(rows[i]['Description']));
					newCat.appendChild(newDesc);
					
					var newDateTime = document.createElement("span");
					newDateTime.setAttribute('class','datetime');
					newDateTime.appendChild(document.createTextNode(rows[i]['Date']+" "+rows[i]['Time']));
					newCat.appendChild(newDateTime);
					
					categories.appendChild(newCat);
				}
				document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Successfully added nodejam</a>";
				var loc = window.location.href,
				index = loc.indexOf('#');
				if (index > 0) {
					window.location.replace("#");
					if (typeof window.history.replaceState == 'function') {
						history.replaceState({},'',window.location.href.slice(0,-1));
					}
				}
			} else {
				document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Error adding nodejam</a>";
				var loc = window.location.href,
				index = loc.indexOf('#');
				if (index > 0) {
					window.location.replace("#");
					if (typeof window.history.replaceState == 'function') {
						history.replaceState({},'',window.location.href.slice(0,-1));
					}
				}
			}
		}
	}
	xmlhttp.open("POST", "addCompetition.php", true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("Name="+name.value+"&Desc="+desc.value+"&Date="+date.value+"&Time="+time.value);
	return false;
}

var i;

function dragOver(ev) {
	ev.preventDefault();
	document.getElementById("delete").setAttribute("class","dragover");
}

function dragLeave(ev) {
	ev.preventDefault();
	document.getElementById("delete").removeAttribute("class");
}

function drag(ev)
{
	ev.dataTransfer.setData("Text",ev.target.id);
}

function drop(ev)
{
document.getElementById("delete").removeAttribute("class");
	var catLen = 0;
	var cats = document.getElementById("categories").childNodes;
	for (var i = 0; i < cats.length-1; i++) {
		if (cats[i].tagName == "LI") {
			catLen++;
		}
	}
	if (catLen > 1) {
		ev.preventDefault();
		var id = ev.dataTransfer.getData("Text");
		var el = document.getElementById(id);
		var title = el.childNodes[1].innerText;
		//run ajax to update db
		id = id.replace("cat","");
		var xmlhttp;
		if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else { // code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				if (xmlhttp.responseText == "Deleted") {
					document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Successfully deleted event</a>";
					var loc = window.location.href,
					index = loc.indexOf('#');
					if (index > 0) {
						window.location.replace("#");
						if (typeof window.history.replaceState == 'function') {
							history.replaceState({},'',window.location.href.slice(0,-1));
						}
					}
				} else {
					document.getElementById("alert").innerHTML = "<a class='alert' href='#alert'>Error deleting event. Please reload the page and try again.</a>";
					var loc = window.location.href,
					index = loc.indexOf('#');
					if (index > 0) {
						window.location.replace("#");
						if (typeof window.history.replaceState == 'function') {
							history.replaceState({},'',window.location.href.slice(0,-1));
						}
					}
				}
			}
		}
		xmlhttp.open("POST", "updateEvent.php", true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("a=d&b="+id);
		el.remove();
	}
}
</script>
	</body>
</html>