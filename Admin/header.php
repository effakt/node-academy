<?php	
	include '../includes/functions.inc.php';
	include '../includes/db.inc.php';
	include '../includes/access.inc.php';
	
	$Name = "Node.JS Academy";
	
	$page = basename($_SERVER['PHP_SELF']);
	switch ($page) {
		case 'index.php':
			$page = 'Admin';
		break;
		case 'users.php':
			$page = 'Users';
		break;
		case 'todo.php':
			$page = 'Todo';
		break;
		case 'nodejam.php':
			$page = 'NodeJam';
		break;
		case 'categories.php':
			$page = 'Categories';
		break;
		case 'lessons.php':
			$page = 'Lessons';
		break;
		case 'errors.php':
			$page = 'Errors';
		break;
		default:
			$page = '404 Page Not Found';
		break;
	}
	
	include 'header.html.php';
?>