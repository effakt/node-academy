<!DOCYTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="style.css" />
	<title><?php echo $page; ?> - <?php echo $Name; ?></title>
	</head>
	<body>
	
<div id='alert'></div>
		<div id="adminBar">
			<ul>
				<li class="logo">
					<a href="./">
					<img height="30px" src=""></img>
					</a>
				</li>
				<li>
					<a href="../">
						<span>Node.JS Academy</span>
					</a>
					<ul>
						<li>
						<a href="../">
						Visit Academy
						</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="users">
						<span>Users</span>
					</a>
					<ul>
						<li>
						<a href="users">
						View Users
						</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="todo">
						<span>Todo</span>
					</a>
					<ul>
						<li>
						<a href="todo">
						View Todo List
						</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="nodejam">
						<span>NodeJam</span>
					</a>
					<ul>
						<li>
						<a href="nodejam">
						View NodeJam
						</a>
						</li>
						<li>
						<a href="nodejam#addForm">
						Start New NodeJam
						</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="categories">
						<span>Categories</span>
					</a>
					<ul>
						<li>
						<a href="categories">
						View Categories
						</a>
						</li>
						<li>
						<a href="categories#addForm">
						Add New Category
						</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="lessons">
						<span>Lessons</span>
					</a>
					<ul>
						<li>
						<a href="lessons">
						View Lessons
						</a>
						</li>
						<li>
						<a href="lessons#addForm">
						Add New Lesson
						</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="errors">
						<span>Errors</span>
					</a>
					<ul>
						<li>
						<a href="errors">
						View Errors
						</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>