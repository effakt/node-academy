<?php
	include 'header.php';		
	if (isset($_SESSION['u']) && !empty($_SESSION['u'])) {
		if ($userRole == "Admin") {
			//get categories
			try {
				$result = $pdo->query("SELECT * FROM lessoncategory");
			} catch (PDOException $e) {
			exception($result->errorInfo(), $e);
				die('Error fetching categories from the database!');
			}
			$categories = $result->fetchAll();
			include 'categories.html.php';
		} else {
			echo "Access Denied";
		}
	} else {
		header("location: ./");
	}
?>