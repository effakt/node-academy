<?php
	include 'header.php';
	if (isset($_GET['del'])) {
		try {
			$result = $pdo->query("DELETE FROM errors");
		} catch (PDOException $e) {
			die("Unable to delete errors");
			exception($result->errorInfo(), $e);
		}
	}
	try {
		$result = $pdo->query("SELECT * FROM errors ORDER BY Date DESC");
		$errors = $result->fetchAll();
	} catch (PDOException $e) {
		die("Unable to fetch errors");
		exception($result->errorInfo(), $e);
	}
	include 'errors.html.php';
?>