<?php
	include 'header.php';		
	if (isset($_SESSION['u']) && !empty($_SESSION['u'])) {

		if ($userRole == "Admin") {
			//get users
			try {
				$result = $pdo->query("SELECT u.* FROM users u WHERE u.Active = 1");
			} catch (PDOException $e) {
				exception($result->errorInfo(), $e);
				die('Error fetching users from the database!');
			}
			$users = $result->fetchAll();
			include 'users.html.php';
		} else {
			echo "Access Denied";
		}
	} else {
		header("location: ./");
	}
?>