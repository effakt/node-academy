<?php
	include '../includes/access.inc.php';
	include '../includes/functions.inc.php';
	include '../includes/db.inc.php';
	foreach($_POST AS $key => $value) {
		if (empty($value)) {
			die("Error: ".$key." is not valid");
		}
	}
	try {
		$result = $pdo->prepare("INSERT INTO lessoncategory (Name) VALUES (:name)");
		$result->bindParam(":name", $_POST['Name']);
		$result->execute();
		$result = $pdo->prepare("INSERT INTO medals (Category) VALUES (:category)");
		$result->bindParam(":category", $pdo->lastInsertId());
		$result->execute();
		echo "Added<:::>";
	} catch (PDOException $e) {
		exception($result->errorInfo(), $e);
		die("Error: Unable to update categories");
	}
	try {
		$result = $pdo->query("SELECT * FROM lessoncategory");
	} catch (PDOException $e) {
		exception($result->errorInfo(), $e);
		die("Error: Unable to fetch categories");
	}
	echo json_encode($result->fetchAll());
?>