<?php
	include '../includes/functions.inc.php';
	include '../includes/db.inc.php';
	include '../includes/access.inc.php';
	if ($userRole == "Admin") {	
		if (isset($_POST['a'])) {
			if ($_POST['a'] == 'a') {		
				 try {
					$result = $pdo->prepare("INSERT INTO todo (`Value`) VALUES (:value)");
					$result->bindParam(':value', $_POST['b']);
					$result->execute();
					echo "Added<:::>";
				} catch (PDOException $e) {
					exception($result->errorInfo(), $e);
					die('Error: Unable to add todo!');
				} 
				try {
					$result = $pdo->query("SELECT * FROM todo");
					echo json_encode($result->fetchAll());
				} catch (PDOException $e) {
					die('Error: Unable to fetch todo!');
					
					exception($result->errorInfo(), $e);
				}
			} elseif ($_POST['a'] == 'd') {
				try {
					$delIds = json_decode($_POST['b']);
					$sql = "DELETE FROM todo WHERE";
					foreach ($delIds AS $delId) {
						if ($delId == end($delIds))
							$sql .= " ID = ".$delId;
						else
							$sql .= " ID = ".$delId." OR";
					}
					$pdo->query($sql);
					echo "Deleted";
				} catch (PDOException $e) {
					exception($result->errorInfo(), $e);
					die('Error: Unable to delete todo!');
				}
			} elseif ($_POST['a'] == 'u') {
				try {
					$result = $pdo->prepare("UPDATE todo SET Checked = :checked WHERE `ID` = :id");
					$result->bindParam(':id', $_POST['b']);
					$result->bindParam(':checked', $_POST['c']);
					$result->execute();
					echo "Updated";
				} catch (PDOException $e) {
					exception($result->errorInfo(), $e);
					die('Error: Unable to update todo!');
				}
			}
		}
	}
?>