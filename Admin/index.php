<?php
	include 'header.php';
	if (isset($_SESSION['u']) && !empty($_SESSION['u'])) {
		$recentTopics = Array();
		$recentComments = Array();
		$recentRep = Array();
		$recentPosts = Array();
		if ($userRole == "Admin") {
		//get general stats
			try {
				$result = $pdo->query("SELECT (SELECT COUNT(*) FROM topics) AS Topics, (SELECT COUNT(*) FROM posts) AS Comments, (SELECT COUNT(*) FROM users) AS Users");
				$counts = $result->fetch();
			} catch (PDOException $e) {
			
				exception($result->errorInfo(), $e);
				die('Error: Unable to fetch General Stats');
			}
			//get Recent Activity
			try
			{
				//get topics
				$result = $pdo->query("SELECT ID, Name AS Subject, Date, (SELECT Name FROM users WHERE ID = t.Author) AS Actor, (SELECT Name FROM lessoncategory WHERE ID = t.Category) AS Parent FROM topics t LIMIT 15");
			}
			catch (PDOException $e)
			{
				exception($result->errorInfo(), $e);
				die('Error: Unable to fetch user topics');
			}
			foreach ($result AS $row) {
				$row[] = "topic";
				$recentTopics[] = $row;
			}
			try
			{
				//get comments
				$result = $pdo->query("SELECT c.ID, c.Date, c.Topic, t.Name, (SELECT Name FROM users WHERE ID = c.Author) AS Actor, (SELECT Name FROM topics WHERE ID = c.Topic) AS Parent, (SELECT Name FROM topics WHERE ID = c.Topic) AS Subject FROM posts c INNER JOIN topics t ON c.Topic = t.ID LIMIT 15");
			}
			catch (PDOException $e)
			{
				exception($result->errorInfo(), $e);
				die("Error: Unable to fetch comments");
			}
			foreach ($result AS $row) {
				$row[] = "comment";
				$recentComments[] = $row;
			}
			$recentPosts = array_merge($recentTopics,$recentComments);
			function cmp($a, $b) {
				$a = strtotime($a['Date']);
				$b = strtotime($b['Date']);
				if ($a == $b) {
					return 0;
				}
				return ($a < $b) ? 1 : -1;
			}
			if (count($recentPosts) > 0) {
				usort($recentPosts, "cmp");
				$recentPosts = array_slice($recentPosts, 0, 15);
			}
			//get admin notes
			try {
				$result = $pdo->query("SELECT Value from settings WHERE Name = 'Notes'");
			}
			catch (PDOException $e)
			{
				
				exception($result->errorInfo(), $e);
				die('Error: Unable to fetch admin notes from the database!');
			}
			$notes = $result->fetchColumn();
			
			include 'index.html.php';
		} else {
			echo "Access Denied";
		}
	} else {
	
		header("Location: ../");
	}
?>