<?php
include 'includes/db.inc.php';
include 'includes/access.inc.php';
$i = $_GET['i'];
if (!isset($_SESSION['u']) && empty($_SESSION['u'])) {
	if ($i >= '5') {
		die("Please Register<:::><p>We hope you have enjoyed this free preview of Node.JS Academy.<br/> If you would Like to continue, please register an account.</p>");
	}
}
$prevI = $i-1;
try {
	$result = $pdo->prepare("SELECT ID, Category FROM lessons WHERE Category = (SELECT Category FROM lessons WHERE ID = :id) ORDER BY ID DESC LIMIT 1");
	$result->bindParam(":id", $prevI);
	$result->execute();
	$results = $result->fetch();
	$lastLessonId = $results[0];
	$category = $results[1];
} catch (PDOException $e) {
	exception($result->errorInfo(), $e);
	die("Error: Unable to fetch last lesson in category");
}
if ($prevI == $lastLessonId) {
	try {
		$result = $pdo->prepare("INSERT INTO usermedals (User, Medal) VALUES (:user, :medal) ON DUPLICATE KEY UPDATE ID=ID");
		$result->bindParam(":user", $userId);
		$result->bindParam(":medal", $category);
		$result->execute();
	} catch (PDOException $e) {
		exception($result->errorInfo(), $e);
		die("Error: Unable to add medal");
	}
}
try {
	$result = $pdo->prepare("SELECT Complete FROM users WHERE ID = :id");
	$result->bindParam(":id", $userId);
	$result->execute();
	$userComplete = (bool)$result->fetchColumn();
} catch (PDOException $e) {
	exception($result->errorInfo(), $e);
	die("Error: Unable to fetch user complete");
}
if ($userComplete)
	die("There are no more lessons<:::><p>Congratulations! You have completed all lessons in Node.JS Academy!.<br/>More lessons may be added at a later date.<br/>Now that you have completed every lesson, you can take part in the NodeJam! Simply click on NodeJam in the navigation to see when the next competition will be held if you wish to compete!</p>");

try {
	$result = $pdo->query("SELECT COUNT(*) FROM lessons");
	$numLessons = $result->fetchColumn();
} catch (PDOException $e) {
	exception($result->errorInfo(), $e);
	die("Error: Unable to fetch number of lessons");
}
if ($i > $numLessons) {
try {
	$result = $pdo->prepare("UPDATE users SET Complete = 1 WHERE ID = :id");
	$result->bindParam(":id", $userId);
	$result->execute();
} catch (PDOException $e) {
	exception($result->errorInfo(), $e);
	die("Error: Unable to set complete");
}
	die("There are no more lessons<:::><p>Congratulations! You have completed all lessons in Node.JS Academy!.<br/>More lessons may be added at a later date.<br/>Now that you have completed every lesson, you can take part in the NodeJam! Simply click on NodeJam in the navigation to see when the next competition will be held if you wish to compete!</p>");
} 
try
{
	$result = $pdo->prepare("SELECT * FROM lessons WHERE ID = :i");
	$result->bindParam(':i', $i);
	$result->execute();
}
catch (PDOException $e)
{
	exception($result->errorInfo(), $e);
	die('Error: Unable to fetch lesson from the database!');
}
foreach ($result as $row) {
	echo $row[1]."<:::>".$row[2]."<:::>".$row[3]."<:::>".$row[4]."<:::>".$row[5]."<:::>".$row[0];
}
try {
	$result = $pdo->prepare("SELECT Progress FROM users WHERE ID = :id");
	$result->bindParam(":id", $userId);
	$result->execute();
	$userProgress = $result->fetchColumn();
} catch (PDOException $e) {
	exception($result->errorInfo(), $e);
	die("Error: Unable to fetch user progress");
}
if ($i > $userProgress) {
	try {
		$result = $pdo->prepare("UPDATE users SET Progress = :i WHERE ID = :id");
		$result->bindParam(":i", $i);
		$result->bindParam(":id", $userId);
		$result->execute();
	} catch (PDOException $e) {
		exception($result->errorInfo(), $e);
		die("Error: Unable to update user progress");
	}
}
?>