<?php
include 'includes/access.inc.php';
include 'includes/db.inc.php';
include 'includes/functions.inc.php';
 if (empty($_POST)) {
	try {
		$result = $pdo->query("SELECT ID FROM competitions WHERE Date BETWEEN DATE_SUB(NOW(), INTERVAL 96 HOUR) AND NOW()");
		$compId = $result->fetchColumn();
	} catch (PDOException $e) {
	exception($result->errorInfo(), $e);
		die("Error: Unable to fetch competition ID");
	}
	try {
		$result = $pdo->prepare("SELECT * FROM competitionparticipants WHERE Competition = :id");
		$result->bindParam(":id", $compId);
		$result->execute();
		$entries = $result->fetchAll();
	} catch (PDOException $e) {
	exception($result->errorInfo(), $e);
		die("Error: Unable to fetch competition entries.");
	}
	include 'voteComp.html.php';
} else {
	 foreach($_POST AS $key => $value) {
		if (empty($value)) {
			die("Error: ".$key." is not valid");
		}
	} 
	try {
		$result = $pdo->query("SELECT ID FROM competitions WHERE Date BETWEEN DATE_SUB(NOW(), INTERVAL 96 HOUR) AND NOW()");
		$compId = $result->fetchColumn();
	} catch (PDOException $e) {
	exception($result->errorInfo(), $e);
		die("Error: Unable to fetch competition ID");
	}
	try {
		$result = $pdo->prepare("INSERT INTO competitionvoting (Competition, User, CompetitionEntry) VALUES (:competition, :user, :competitionentry)");
		$result->bindParam(":competition",$compId);
		$result->bindParam(":user",$userId);
		$result->bindParam(":competitionentry",$_POST['Entry']);
		$result->execute();
		echo "Added";
	} catch (PDOException $e) {
		$err = $result->errorInfo()[1];
		if ($err == 1062) {
			die("Error: You have already voted for a entry.");
		} else {
			exception($result->errorInfo(), $e);
			die("Error: Unable to add vote to the database");
		}
	}
}
?>