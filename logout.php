<?php
	if (isset($_GET) && !empty($_GET)) {
		include '/includes/db.inc.php';
		include '/includes/access.inc.php';
		unset($_SESSION['u']);
		unset($_SESSION['p']);
		unset($_SESSION['r']);
		session_destroy();
		echo "loggedout";
	} else {
		include 'logout.html.php';
	}
?>