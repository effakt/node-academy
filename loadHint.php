<?php
include 'includes/db.inc.php';
$i = $_GET['i'];
try
{
	$result = $pdo->prepare("SELECT Hint FROM hints WHERE LessonID = :i");
	$result->bindParam(':i', $i);
	$result->execute();
}
catch (PDOException $e)
{
	exception($result->errorInfo(), $e);
	die('Error: Unable to fetch hints from the database!');
}
foreach ($result as $row) {
	$hints[] = $row[0];
}
for ($i = 0; $i < count($hints); $i++) {
	if ($i == count($hints)-1) {
		echo $hints[$i];
	} else {
		echo $hints[$i]."<:::>";
	}
}
?>