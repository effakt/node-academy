<?php
$a = $_GET['a'];
chdir('scripts');
$a = json_decode($a);
$time = round(microtime(true) * 1000);
$data = "";
$file = $time.'.js';
$handle = fopen($file, 'w') or die('Cannot open file:  '.$file);
if (is_array($a)) {
	foreach ($a as $b) {
		$data .= $b."\r\n";
	}
}
preg_match('/(listen\s*\((\d*)\))/', $data, $port);
$port = $port[(count($port)-1)];

fwrite($handle, $data);
fclose($handle);


$h = popen("start node ".$file, "r");
// Create a curl handle
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $_SERVER['HTTP_HOST'].':'.$port);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
$output = curl_exec($curl);
$errno = curl_errno($curl);
$err = curl_error($curl);
curl_close($curl);

pclose($h);
exec('taskkill /F /IM node.exe');
  if ($errno > 0 && $errno != 56) {
	exec("node ".$file." 2>&1", $output1, $return1);
	foreach ($output1 as $error) {
		if (trim($error) != "") {
			$errors[] = trim($error);
		}
	}
	//get Error Line
	$errorLine = explode(':',$errors[0]);
	$errorLine = end($errorLine);
	echo $errorLine."<:::>";
	
	//get Syntax Error
	$syntaxError = $errors[3];
	echo $syntaxError;
	
} else { 
	echo $output;
} 

unlink($file);

?>