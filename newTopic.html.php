<p id="catId" hidden><?php echo $_GET['i'] ?></p>
<header class="supportForum">
	<span class="back" onclick="forumBack('nTopic')"><</span>
	<h2>Post New Topic</h2>
</header>
<div class="NewForm">
	<input type="text" class="fullSize" placeholder="Type topic subject here" required/>
	<textarea placeholder="Type topic content here" class="fullSize" required></textarea>
	<div class='postReply' onclick="newPost(this.parentNode, 'Topic')">
		<span>Post <b>Topic</b></span>
	</div>
</div>