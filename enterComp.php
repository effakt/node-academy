<?php
if (empty($_POST)) {
	include 'enterComp.html.php';
} else {
	include 'includes/access.inc.php';
	include 'includes/db.inc.php';
	include 'includes/functions.inc.php';
	foreach($_POST AS $key => $value) {
		if (empty($value)) {
			die("Error: ".$key." is not valid");
		}
	}
	if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $_POST['RepoUrl'])) {
		die("Error: RepoUrl must be a valid url");
	}
	try {
		$result = $pdo->query("SELECT ID FROM competitions WHERE Date > NOW() ORDER BY Date ASC LIMIT 1");
		$compId = $result->fetchColumn();
	} catch (PDOException $e) {
	exception($result->errorInfo(), $e);
		die("Error: Unable to fetch competition ID");
	}
	try {
		$result = $pdo->prepare("INSERT INTO competitionparticipants (Competition, User, Title, Description, Repo, RepoUrl) VALUES (:competition, :user, :title, :description, :repo, :repourl)");
		$result->bindParam(":competition",$compId);
		$result->bindParam(":user",$userId);
		$result->bindParam(":title",$_POST['Title']);
		$result->bindParam(":description",$_POST['Description']);
		$result->bindParam(":repo",$_POST['Repo']);
		$result->bindParam(":repourl",$_POST['RepoUrl']);
		$result->execute();
		echo "Added";
	} catch (PDOException $e) {
		$err = $result->errorInfo()[1];
		if ($err == 1062) {
			die("Error: You are already in this competition");
		} else {
			exception($result->errorInfo(), $e);
			die("Error: Unable to add user to the database");
		}
	}
}
?>