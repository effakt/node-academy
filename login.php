<?php
	if (isset($_POST) && !empty($_POST)) {
		include 'includes/access.inc.php';
		include 'includes/db.inc.php';
		include 'includes/functions.inc.php';
		foreach($_POST AS $key => $value) {
			if (empty($value)) {
				die("Error: ".$key." is not valid");
			}
		}
		$password = pass($_POST['Password']);
		try { 
			$result = $pdo->prepare("SELECT * FROM users WHERE Name = :Name");
			$result->bindParam(":Name", $_POST['Username']);
			$result->execute();
			$numRows = $result->rowCount();
		} catch (PDOException $e) {
			exception($result->errorInfo(), $e);
			die("Error: Unable to fetch users");
		}
		if ($numRows == 0) {
			die("Error: Username does not exist");
		} else {
			$user = $result->fetch();
			if ($user['Password'] == $password) {
				$r = $result->fetchColumn();
				$_SESSION['u'] = $_POST['Username'];
				$_SESSION['p'] = $password;
				$_SESSION['r'] = $user['Role'];
				echo "loggedin";
			} else {
				echo "Error: Password is incorrect";
			}
		}
	} else {
		include 'login.html.php';
	}
?>