<p id="catId" hidden><?php echo $topicInfo['Category'] ?></p>
<p id="topicId" hidden><?php echo $_GET['i'] ?></p>
<header class="supportForum">
	<span class="back" onclick="forumBack('vTopic')"><</span>
	<h2><?php echo $topicName ?></h2>
</header>
<table class='supportForumThread'>
	<tbody>
		<tr>
			<td>
			<div class="medals right">
				<div class="arrow"></div>
				<h3 class="medals-title"><?php echo $topicInfo['Author'] ?> Medals</h3>
				<div class="medals-content">
					<?php foreach ($topicInfo[8] AS $medal) { ?>
					<img src="medals/<?php echo $medal['Medal'] ?>.png" height="50px" width="50px" alt="medal<?php echo $medal['Medal'] ?>"/>
					<?php } ?>
				</div>
			</div>
			<span><?php echo $topicInfo['Author'] ?></span><br/>
			<img src="http://www.gravatar.com/avatar/<?php echo $topicInfo['DisplayPic'] ?>?s=100&r=g&d=mm" height="100px" width="100px"/><br/>
			<span><?php echo $topicInfo['Role'] ?></span>
			</td>
			<td><?php if ($userRole == "Mod" || $userRole == "Admin") { ?><span class="meta"><span onclick="enableEditPost(this, 'topic')">Edit</span> <span onclick="deletePost(this, 'topic')">Delete</span></span><?php } ?>
			<p hidden><?php echo $topicInfo['ID'] ?></p>
			<span><?php echo nl2br($topicInfo['Content']) ?></span>
			</td>
		</tr>
		<?php foreach ($posts AS $post) { ?>
		<tr>
			<td>
			<div class="medals right">
				<div class="arrow"></div>
				<h3 class="medals-title"><?php echo $post['Author'] ?> Medals</h3>
				<div class="medals-content">
					<?php foreach ($post[7] AS $medal) { ?>
					<img src="medals/<?php echo $medal['Medal'] ?>.png" height="50px" width="50px" alt="medal<?php echo $medal['Medal'] ?>"/>
					<?php } ?>
				</div>
			</div>
			<span><?php echo $post['Author'] ?></span><br/>
			<img src="http://www.gravatar.com/avatar/<?php echo $post['DisplayPic'] ?>?s=100&r=g&d=mm" height="100px" width="100px"/><br/>
			<span><?php echo $post['Role'] ?></span>
			</td>
			<td><?php if ($userRole == "Mod" || $userRole == "Admin") { ?><span class="meta"><span onclick="enableEditPost(this, 'post')">Edit</span> <span onclick="deletePost(this, 'post')">Delete</span></span><?php } ?>
			<p hidden><?php echo $post['ID'] ?></p>
			<span><?php echo nl2br($post['Content']) ?></span>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<table class='supportForumThread'>
	<tbody>
		<tr>
			<td>
			<span><?php echo $_SESSION['u'] ?></span><br/>
			<img src="http://www.gravatar.com/avatar/<?php echo $userDisplayPic ?>?s=100&r=g&d=mm" height="100px" width="100px"/><br/>
			<span><?php echo $_SESSION['r'] ?></span>
			</td>
			<td><textarea placeholder="Type your reply here"></textarea>
			<div class='postReply' onclick="newPost(this.parentNode, 'Comment')">
				<span>Post <b>Reply</b></span>
			</div></td>
		</tr>
	</tbody>
</table>