<?php
	include 'includes/access.inc.php';
	include 'includes/db.inc.php';
	$allTopics = array();
	try {
		$result = $pdo->prepare("SELECT Name FROM lessoncategory WHERE ID = :id");
		$result->bindParam(":id", $_GET['i']);
		$result->execute();
	}	catch (PDOException $e) {
		echo "Error: Unable to fetch category name";
	}
	$catName = $result->fetchColumn();
	try {
		$result = $pdo->prepare("SELECT t.ID, t.Name, t.Date, u.DisplayName AS Author FROM topics t INNER JOIN users u ON t.Author = u.ID WHERE t.Category = :id");
		$result->bindParam(":id", $_GET['i']);
		$result->execute();
	} catch	(PDOException $e) {
		echo "Error: Unable to fetch topics";
	}
	$topics = $result->fetchAll();
	if (count($topics) > 0) {
		foreach ($topics AS $topic) {
			try {
				$result = $pdo->prepare("SELECT p.ID, p.Date, u.DisplayName AS Author FROM posts p INNER JOIN users u ON p.Author = u.ID INNER JOIN topics t ON t.ID = p.Topic WHERE t.ID = :id ORDER BY p.Date DESC LIMIT 1");
				$result->bindParam(":id", $topic['ID']);
				$result->execute();
			} catch (PDOException $e) {
				echo "Error: Unable to fetch posts";
			}
			$lastComment = $result->fetch();
			if (count($lastComment) > 0) {
				if ($topic['Date'] < $lastComment['Date'])
					$topic['LastComment'] = $lastComment;
			}
			$allTopics[] = $topic;
		}
		$topics = $allTopics;
	}
	include 'forumIndex.html.php';
?>