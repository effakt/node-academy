<?php
include 'includes/access.inc.php';
include 'includes/db.inc.php';
include 'includes/functions.inc.php';
if (!isset($_GET['ajax'])) {
	if (isset($_GET['a']) && !empty($_GET['a']) && isset($_GET['b']) && !empty($_GET['b'])) {
		try {
			$result = $pdo->prepare("SELECT Code FROM users WHERE Email = :email");
			$result->bindParam(':email', $_GET['a']);
			$result->execute();
		} catch (PDOException $e) {
			exception($result->errorInfo(), $e);
			die('Error: Unable to fetch Code from the database!');
		}
		$ActivationCode = $result->fetchColumn();
		if ($ActivationCode == $_GET['b']) {
			try {
				$result = $pdo->prepare("UPDATE users SET Active = '1' WHERE Email = :email");
				$result->bindParam(':email', $_GET['a']);
				$result->execute();
			} catch (PDOException $e) {
				exception($result->errorInfo(), $e);
				die('Error updating status in the database!');
			}
			if (isset($_GET['ajax'])) {
				echo "Activated";
			} else {
				header("Location: .");
			}
		} else {
			echo "Error: Activation code is incorrect.";
		}
	} else {
		if (!isset($_GET['a']) || empty($_GET['a'])) {
			echo "Error: No email was supplied.";
		} else {
			echo "Error: No Activation code was supplied.";
		}
	}
} else {
	if (isset($_POST['b'])) {
		if (empty($_POST['b'])) {
			die("Error: ActivationCode must be set");
		} else {
			try {
				$result = $pdo->prepare("SELECT Code FROM users WHERE Name = :name");
				$result->bindParam(":name", $_SESSION['u']);
				$result->execute();
				$activationCode = $result->fetchColumn();
			} catch (PDOException $e) {
				exception($result->errorInfo(), $e);
				die("Error: ActivationCode was unable to be fetched");
			}
			if ($activationCode == $_POST['b']) {
				try {
					$result = $pdo->prepare("UPDATE users SET Active = 1 WHERE Name = :name");
					$result->bindParam(":name", $_SESSION['u']);
					$result->execute();
					echo "Activated";
				} catch (PDOException $e) {
					exception($result->errorInfo(), $e);
					die("Error: ActivationCode failed to update user");
				}
			} else {
				die("Error: ActivationCode is not valid");
			}
		}
	} else {
		include 'activation.html.php';
	}
}
?>