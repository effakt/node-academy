<?php
	include 'includes/functions.inc.php';
	include 'includes/db.inc.php';
	include 'includes/access.inc.php';
	$isNextComp = false;
	$voting = false;
	if (!isset($_SESSION['u']) && empty($_SESSION['u'])) {
		header("Location: ./");
	}
	try {
		$result = $pdo->query("SELECT Date FROM competitions WHERE Date BETWEEN DATE_SUB(NOW(), INTERVAL 96 HOUR) AND NOW()");
		$runningComp = $result->fetchColumn();
		$isRunning = (empty($runningComp)) ? false : true;
	} catch (PDOException $e) {
	exception($result->errorInfo(), $e);
		die("Error: Unable to fetch current competition");
	}
	if ($isRunning) {
		try {
			$result = $pdo->query("SELECT Date FROM competitions WHERE Date BETWEEN DATE_SUB(NOW(), INTERVAL 60 HOUR) AND NOW()");
			$voting = $result->fetchColumn();
			$voting = (empty($voting) ? false : true);
		} catch (PDOException $e) {
		exception($result->errorInfo(), $e);
			die("Error: Unable to fetch current competition status");
		}
	} else {
		try {
			$result = $pdo->query("SELECT Date FROM competitions WHERE Date > NOW() ORDER BY Date ASC LIMIT 1");
			$nextComp = $result->fetchColumn();
		} catch (PDOException $e) {
		exception($result->errorInfo(), $e);
			die("Error: Unable to fetch next competitions");
		}
		$isNextComp = (empty($nextComp)) ? false : true;
	}
	if ($isNextComp) {
		try {
			$result = $pdo->prepare("SELECT Progress FROM users WHERE ID = :id");
			$result->bindParam(":id",$userId);
			$result->execute();
			$userProgress = $result->fetchColumn();
		} catch (PDOException $e) {
		exception($result->errorInfo(), $e);
			die("Error: Unable to fetch user progress");
		}
		try {
			$result = $pdo->query("SELECT COUNT(*) FROM lessons");
			$lessons = $result->fetchColumn();
		} catch (PDOException $e) {
		exception($result->errorInfo(), $e);
			die("Error: Unable to fetch lessons count");
		}
		$canEnter = ($userProgress == $lessons) ? true : false;
	}
	if ($voting) {
		try {
			$result = $pdo->prepare("SELECT cp.*, u.DisplayName AS uUser FROM competitionparticipants cp INNER JOIN competitions c ON cp.Competition = c.ID INNER JOIN users u ON cp.User = u.ID WHERE c.Date = :date");
			if ($isRunning) {
				$result->bindParam(":date", $runningComp);
			}
			$result->execute();
			$compUsers = $result->fetchAll();
		} catch (PDOException $e) {
		exception($result->errorInfo(), $e);
			die("Error: unable to fetch competition users");
		}
		for ($i = 0; $i < count($compUsers); $i++) {
			try {
				$result = $pdo->prepare("SELECT COUNT(*) FROM competitionvoting WHERE CompetitionEntry = :id");
				$result->bindParam(":id", $compUsers[$i]['ID']);
				$result->execute();
				$votes = $result->fetchColumn();
				$votes = (empty($votes)) ? 0 : $votes;
				$compUsers[$i]['Votes'] = $votes;
			} catch (PDOException $e) {
			exception($result->errorInfo(), $e);
				die("Error: unable to fetch entry votes");
			}
		}
	}
	try {
		$result = $pdo->query("SELECT cp.*, u.DisplayName AS uUser, c.ID AS cID, max((SELECT COUNT(*) FROM competitionvoting cv WHERE cv.CompetitionEntry = cp.ID GROUP BY cv.Competition)) AS Votes FROM competitionparticipants cp INNER JOIN competitions c ON cp.Competition = c.ID INNER JOIN users u ON cp.User = u.ID ORDER BY c.ID DESC");
		$pastWinners = $result->fetchAll();
	} catch (PDOException $e) {
	exception($result->errorInfo(), $e);
		die("Unable to fetch past winners");
	}
	include 'competitions.html.php';
?>