<?php include 'includes/access.inc.php'; ?>
<header class="supportForum">
	<h2>Enter NodeJam</h2>
</header>
<section class="signup">
	<div class="form signupForm"  onkeypress="if (event.keyCode == 13) { enterComp(this) }">
		<div class="Error"></div>
		<div>
			<label for="Repo">Git Repository:</label>
			<select id="Repo">
				<option value="BitBucket">BitBucket</option>
				<option value="GitHub">GitHub</option>
			</select>
		</div>
		<div>
			<label for="RepoUrl">Repository Url:</label>
			<input type="text" id="RepoUrl"/>
		</div>
		<div>
			<label for="Title">Project Title:</label>
			<input type="text" id="Title"/>
		</div>
		<div>
			<label for="Description">Description:</label>
			<textarea id="Description"></textarea>
		</div>
		<div class='postNew' onclick="enterComp(this.parentNode);">
			<span>Enter <b>NodeJam</b></span>
		</div>
	</div>
</section>