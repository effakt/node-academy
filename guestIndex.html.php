<html>
<head>
<!-- <link href="google-code-prettify/prettify.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="google-code-prettify/prettify.js"></script> -->

<script src="codemirror/lib/codemirror.js"></script>
<script src="codemirror/javascript/javascript.js"></script>
<link rel="stylesheet" href="codemirror/lib/codemirror.css"/>

<link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
<div id="Wrapper">
<header>
<h1><a href="./">Node Acedemy</a></h1>
<nav id="resNav">
	<a onclick="showNav(event);">&#9776;</a>
		<nav id="resNavDrop">
			<a onclick="toggleModal(this.parentNode,'about')">About</a>
			<a onclick="toggleModal(this.parentNode,'login')">Log In</a>
			<a onclick="toggleModal(this.parentNode,'signup')">Sign Up</a>
		</nav>
</nav>
<nav>
	<a onclick="toggleModal(this.parentNode,'about')">About</a>
	<a onclick="toggleModal(this.parentNode,'login')">Log In</a>
	<a onclick="toggleModal(this.parentNode,'signup')">Sign Up</a>
</nav>
</header>
<section>
	<div class="console">
		<span class="c-circle"></span>
		<span class="c-circle"></span>
		<span class="c-circle"></span>
		<div class="c-content">
		</div>
		<div class="c-input">
			<span class="c-inputspan"><input type="text" class="c-inputbox" onkeyup="keyup(event)"/></span>
		</div>
	</div>
</section>
<div id="modal"></div>
<div id="dialog">
<div class="close" onclick="toggleModal()">X</div>
	<div id="forum"></div>
</div>
<footer>
<span>&copy; 2014 Node Academy<br/>Developed by <a href="http://effakt.info/">EffakT Development</a></span>
</footer>
</div>
<script type="text/javascript" src="console.js"></script>
</body>
</html>