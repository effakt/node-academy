<?php
	include 'includes/db.inc.php';
	include 'includes/access.inc.php';
	if (isset($_SESSION['u']) && !empty($_SESSION['u'])) {
		try {
			$result = $pdo->prepare("SELECT Progress FROM users WHERE Name = :Name");
			$result->bindParam(":Name", $_SESSION['u']);
			$result->execute();
			$userProgress = $result->fetchColumn();
			if ($userProgress == 0)
				echo 1;
			else
				echo $userProgress;
		} catch (PDOException $e) {
			echo "1";
		}
	} else {
		echo "1";
	}
?>