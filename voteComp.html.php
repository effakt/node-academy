<?php include 'includes/access.inc.php'; ?>
<header class="supportForum">
	<h2>Vote for your favourite NodeJam project</h2>
</header>
<section class="signup">
	<div class="form signupForm"  onkeypress="if (event.keyCode == 13) { voteComp(this) }">
		<div class="Error"></div>
		<div>
			<label for="Entry">Entries:</label>
			<select id="Entry">
			<option value="" disabled selected>Select one entry</option>
			<?php foreach ($entries AS $entry) { ?>
				<option value="<?php echo $entry['ID'] ?>"><?php echo $entry['Title'] ?></option>
			<?php } ?>
			</select>
		</div>
		<div class='postNew' onclick="voteComp(this.parentNode);">
			<span>Enter <b>NodeJam</b></span>
		</div>
	</div>
</section>