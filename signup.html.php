<header class="supportForum">
	<h2>Register</h2>
</header>
<section class="signup">
	<div class="form signupForm"  onkeypress="if (event.keyCode == 13) { signUp(this) }">
		<div class="Error"></div>
		<div>
			<label for="Username">Username:</label> <input type="text" id="Username">
		</div>
		<div>
			<label for="Password">Password:</label> <input type="password" id="Password">
		</div>
		<div>
			<label for="ConfirmPassword">Confirm Password:</label> <input type="password" id="ConfirmPassword">
		</div>
		<div>
			<label for="Email">Email:</label> <input type="email" id="Email">
		</div>
		<div>
			<label for="ConfirmEmail">Confirm Email:</label> <input type="email" id="ConfirmEmail">
		</div>
		<div class='postNew' onclick="signUp(this.parentNode);">
			<span>Sign <b>Up</b></span>
		</div>
	</div>
</section>