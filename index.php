<?php
	include 'includes/db.inc.php';
	include 'includes/access.inc.php';
	if (isset($_SESSION['u']) && !empty($_SESSION['u'])) {
		include 'userIndex.php';
	} else {
		include 'guestIndex.php';
	}
?>