<header class="supportForum">
	<h2>Activation</h2>
</header>
<section class="login">
	<div class="form logoutForm" onkeypress="if (event.keyCode == 13) { activate(this) }">
		Welcome to Node.JS Academy!<br/>
		You should have been sent a verification code to your registered email.<br/>
		Please type the code in the box below and click the "Activate Now" button!
		<div class="Error"></div>
		<div>
			<input type="text" id="ActivationCode">
		</div>
		<div class='postNew' onclick="activate(this.parentNode)">
			<span>Activate <b>Now</b></span>
		</div>
	</div>
</section>