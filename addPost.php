<?php
	include 'includes/access.inc.php';
	include 'includes/db.inc.php';
	if ($_GET['a'] == "Topic") {
		$titleValid = true;
		$contentValid = true;
		if (!isset($_POST['title']) ||empty($_POST['title'])) {
			echo "Error: Title empty<br/>";
			$titleValid = false;
		}
		if (!isset($_POST['content']) || empty($_POST['content'])) {
			echo "Error: Content empty<br/>";
			$contentValid = false;
		}
		if ($titleValid && $contentValid) {
			try {
				$result = $pdo->prepare("INSERT INTO topics (Name, Content, Author, Category, Date) VALUES (:title, :content, :author, :catId, NOW())");
				$result->bindParam(":title", $_POST['title']);
				$result->bindParam(":content", $_POST['content']);
				$result->bindParam(":author", $userId);
				$result->bindParam(":catId", $_GET['i']);
				$result->execute();
				echo "Success<:::>".$pdo->lastInsertId();
			} catch (PDOException $e) {
				exception($result->errorInfo(), $e);
				die("Error: Unable to insert into the database");
			}
		} else {
			die();
		}
	} elseif($_GET['a'] == "Comment") {
		if (!isset($_POST['content']) || empty($_POST['content'])) {
			die("Error: Content empty");
		} else {
			try {
				$result = $pdo->prepare("INSERT INTO posts (Content, Author, Topic, Date) VALUES (:content, :author, :topic, NOW())");
				$result->bindParam(":content", $_POST['content']);
				$result->bindParam(":author", $userId);
				$result->bindParam(":topic", $_GET['i']);
				$result->execute();
				echo "Success";
			} catch (PDOException $e) {
				exception($result->errorInfo(), $e);
				die("Error: Unable to insert into the database");
			}
		}
	}
?>