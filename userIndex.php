<?php
include 'includes/db.inc.php';
try {
	$result = $pdo->prepare("SELECT c.ID, c.Name, (SELECT COUNT(*) FROM lessons WHERE Category = c.ID) AS Lessons FROM lessoncategory c");
	$result->execute();
} catch (PDOException $e) {
	exception($result->errorInfo(), $e);
	die("Unable to fetch categories from database");
}
	$categories = $result->fetchAll();
try {
	$result = $pdo->prepare("SELECT ID, Name, Category FROM lessons");
	$result->execute();
} catch (PDOException $e) {
	exception($result->errorInfo(), $e);
	die("Unable to fetch lessons from database");
}
$lessons = $result->fetchAll();
try {
	$result = $pdo->prepare("SELECT Progress FROM users WHERE Name = :Name");
	$result->bindParam(":Name", $_SESSION['u']);
	$result->execute();
	$userProgress = $result->fetchColumn();
} catch (PDOException $e) {
	exception($result->errorInfo(), $e);
	die("Unable to fetch user progress");
}
try {
	$result = $pdo->prepare("SELECT Category FROM lessons WHERE ID = :progress");
	$result->bindParam(":progress", $userProgress);
	$result->execute();
	$progressCat = $result->fetchColumn();
	if (empty($progressCat))
		$progressCat = 1;
} catch (PDOException $e) {
	exception($result->errorInfo(), $e);
	die("Unable to fetch progress category");
}
	include 'userIndex.html.php';
?>