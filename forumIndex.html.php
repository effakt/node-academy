
<p id="catId" hidden><?php echo $_GET['i'] ?></p>
<header class="supportForum">
	<h2><?php echo $catName ?></h2>
</header>
<div class='postNew' onclick="loadPage(this, 'newTopic')">
	<span>New <b>Topic</b></span>
</div>
<table class='supportForum'>
	<thead>
		<tr>
			<td>Topics</td>
			<td>Last post</td>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($topics AS $topic) { ?>
		<tr onclick="loadPage(this, 'viewTopic')" class="<?php echo $topic['ID'] ?>">
			<td><?php echo $topic['Name'] ?><br/><span>by <?php echo $topic['Author'] ?> - <?php echo date("D M j, g:ia",strtotime($topic['Date'])) ?></span></td>
			<?php if (isset($topic['LastComment'])) { ?>
				<td>by <?php echo $topic['LastComment']['Author'] ?><br/><?php echo date("D M j, g:ia",strtotime($topic['LastComment']['Date'])) ?></td>
			<?php } else { ?>
				<td>by <?php echo $topic['Author'] ?><br/><?php echo date("D M j, g:ia",strtotime($topic['Date'])) ?></td>
			<?php } ?>
		</tr>
	<?php } ?>
		
	</tbody>
</table>