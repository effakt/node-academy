<header class="supportForum">
	<h2>Log In</h2>
</header>
<section class="login">
	<div class="form loginForm" onkeypress="if (event.keyCode == 13) { login(this) }">
		<div class="Error"></div>
		<div>
			<label for="Username">Username:</label> <input type="text" id="Username">
		</div>
		<div>
			<label for="password">Password:</label> <input type="password" id="Password">
		</div>
		<div class='postNew' onclick="login(this.parentNode)">
			<span>Log <b>In</b></span>
		</div>
	</div>
</section>