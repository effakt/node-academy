<html>
<head>
<script src="codemirror/lib/codemirror.js"></script>
<script src="codemirror/javascript/javascript.js"></script>
<link rel="stylesheet" href="codemirror/lib/codemirror.css"/>
<link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
	<div id="Wrapper">
		<header>
		<h1><a href="./">Node Acedemy</a></h1>
		<nav id="resNav">
			<a onclick="showNav(event);">&#9776;</a>
			<nav id="resNavDrop">
			<?php if ($userRole == "Admin") { ?>
				<a href="Admin">Admin</a>
			<?php } ?>
				<a href="nodejam">NodeJam</a>
				<a onclick="toggleModal(this.parentNode,'about')">About</a>
				<a onclick="toggleModal(this.parentNode,'profile')">Medals</a>
				<a onclick="toggleModal(this.parentNode,'logout')">Log Out</a>
			</nav>
		</nav>
		<nav>
			<?php if ($userRole == "Admin") { ?>
				<a href="Admin">Admin</a>
			<?php } ?>
			<a href="nodejam">NodeJam</a>
			<a onclick="toggleModal(this.parentNode,'about')">About</a>
			<a onclick="toggleModal(this.parentNode,'profile')">Medals</a>
			<a onclick="toggleModal(this.parentNode,'logout')">Log Out</a>
		</nav>
		</header>
		<aside class="searchSide">
			<div>
				<ul class="accordion">
				<?php foreach ($categories AS $cat) { ?>
					<li id="<?php echo "cat".$cat['ID'] ?>" class="accHead">
						<a onclick="openAccordion(this)"><?php echo $cat['Name'] ?><span><?php echo $cat['Lessons'] ?></span></a>
						<ul class="sub-menu">
						<?php $i = 1; ?>
						<?php foreach ($lessons AS $less) { ?>
							<?php if ($less['Category'] == $cat['ID']) { ?>
								<li id="<?php echo $cat['ID']."-".$less['ID'] ?>" title="<?php echo $less['Name'] ?>"><a <?php if ($less['ID'] <= $userProgress) { ?> onclick="loadLesson(<?php echo $less['ID'] ?>); q=<?php echo $less['ID'] ?>; document.getElementsByClassName('c-content')[0].innerHTML += 'Loading Lesson...'" <?php } else { ?> class="locked" <?php } ?> ><em><?php echo $i ?></em><?php echo $less['Name'] ?></a></li>
								<?php $i++; ?>
							<?php } ?>
						<?php } ?>
						</ul>
					</li>
					<?php } ?>
					<li class="accHead">
						<a onclick="openAccordion(this)">Support Forums<span><?php echo count($categories); ?></span></a>
						<ul class="sub-menu">
							<?php $i = 1; ?>
							<?php foreach ($categories AS $cat) { ?>
								<li id="forumCat-<?php echo $cat['ID'] ?>"><a <?php if ($cat['ID'] <= $progressCat) { ?> onclick="toggleModal(this.parentNode,'forum')" <?php } else { ?> class="locked" <?php } ?>><em><?php echo $i ?></em><?php echo $cat['Name'] ?></a></li>
								<?php $i++; ?>
							<?php } ?>
						</ul>
					</li>
				</ul>
			</div>
		</aside>
		<section>
			<div class="console">
			<span class="c-circle"></span>
			<span class="c-circle"></span>
			<span class="c-circle"></span>
				<div class="c-content"></div>
				<div class="c-input">
					<span class="c-inputspan"><input type="text" class="c-inputbox" onkeyup="keyup(event)"/></span>
				</div>
			</div>
		</section>
		<div id="modal"></div>
		<div id="dialog">
		<div class="close" onclick="toggleModal()">X</div>
			<div id="forum">
				
			</div>
		</div>
		<div class="push"></div>
	</div>
	
	<footer>
		<span>&copy; 2014 Node Academy<br/>Developed by <a href="http://effakt.info/">EffakT Development</a></span>
	</footer>
	<script type="text/javascript" src="console.js"></script>
	</body>
	<?php
	if (isset($_SESSION['u']) && !empty($_SESSION['u']) && !$isUserActive) {
		?>
		<script>
			toggleModal(this, 'activation');
		</script>
		<?php
	}
?>
</html>