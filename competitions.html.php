<html>
<head>
<script src="codemirror/lib/codemirror.js"></script>
<script src="codemirror/javascript/javascript.js"></script>
<link rel="stylesheet" href="codemirror/lib/codemirror.css"/>
<link rel="stylesheet" type="text/css" href="style.css"/>
<link rel="stylesheet" type="text/css" href="competitions.css"/>
</head>
<body>
<div id="Wrapper">
<header>
<h1><a href="./">Node Acedemy</a></h1>
<nav id="resNav">
	<a onclick="showNav(event);">&#9776;</a>
	<nav id="resNavDrop">
		<?php if ($userRole == "Admin") { ?>
			<a href="Admin">Admin</a>
		<?php } ?>
		<a href="nodejam">NodeJam</a>
		<a onclick="toggleModal(this.parentNode,'about')">About</a>
		<a onclick="toggleModal(this.parentNode,'profile')">Profile</a>
		<a onclick="toggleModal(this.parentNode,'logout')">Log Out</a>
	</nav>
</nav>
<nav>
	<?php if ($userRole == "Admin") { ?>
		<a href="Admin">Admin</a>
	<?php } ?>
	<a href="nodejam">NodeJam</a>
	<a onclick="toggleModal(this.parentNode,'about')">About</a>
	<a onclick="toggleModal(this.parentNode,'profile')">Profile</a>
	<a onclick="toggleModal(this.parentNode,'logout')">Log Out</a>
</nav>
</header>
<section>
	<div class="console">
		<span class="c-circle"></span>
		<span class="c-circle"></span>
		<span class="c-circle"></span>
		<div class="c-content">
			<h1>Competitions</h1>
			<div class="c-log">
				Welcome to Node.JS Academy Competitions!<br/>
				These competitions can be entered by anybody who has completed all lessons and challenges.<br/>
				Anybody can watch each participant's github commits, to see what they are doing. Also, if the participant allows, they may livestream the development.<br/>
				Competititors have 60 hours to create the project, then there is 36 hours for voting.<br/>
				All work done on the project must be done by the participant in an open source repository on either GitHub or BitBucket.<br/>
				All work must be done within the 60 hours of the competition. Any commits after this mark, will not be counted.<br/>
				All work must be made from scratch.<br/>
				Commit every 15 minutes (Auto commit scripts are encouraged. Its okay to miss a FEW, but try to stick to it. If you take a break, just note in the commit messages that you are taking a break!)<br/><br/>
				<?php if ($isRunning) { ?>
					<?php if ($voting) { ?>
						<b>Competition has finished. Voting is open.</b><br/>
						<a href="" onclick="toggleModal(this.parentNode,'voteComp'); return false;">Click Here to enter your vote</a>
					<?php } else { ?>
						<b>Current competition is in progress...</b>
					<?php } ?>
				<?php } else { ?>
					<?php if ($isNextComp) { ?>
						Next competion will start in: <span id="countdown"></span><br/>
						<?php if ($canEnter) { ?>
						<a href="" onclick="toggleModal(this.parentNode,'enterComp'); return false;">Click Here to enter</a>
						<?php } ?>
					<?php } else { ?>
						<b>No competition is set to happen yet.</b>
					<?php } ?>
				<?php } ?>
			</div>
			<?php if ($voting) { ?>
			<br/><h1>Competitors</h1>
			<div class="c-log">
				<table class="competitors">
					<thead>
						<tr>
							<th width="50">Username</th>
							<th width="100">Title</th>
							<th width="200">Description</th>
							<th width="100">Repository</th>
							<th width="50">Votes</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($compUsers AS $compUser) { ?>
						<tr>
							<td><?php echo $compUser['uUser'] ?></td>
							<td><?php echo $compUser['Title'] ?></td>
							<td><?php echo $compUser['Description'] ?></td>
							<td><a href="<?php echo $compUser['RepoUrl'] ?>" target="_blank"><img src="<?php echo $compUser['Repo'] ?>.png" width="90px"/></a></td>
							<td><?php echo $compUser['Votes'] ?></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
			<?php } ?>
			<br/><h1>Past Winners</h1>
			<div class="c-log">
				<table class="competitors">
					<thead>
						<tr>
							<th></th>
							<th width="50">Username</th>
							<th width="100">Title</th>
							<th width="200">Description</th>
							<th width="100">Repository</th>
							<th width="50">Votes</th>
						</tr>
					</thead>
					<tbody>
					<?php if (empty($pastWinners)) { ?>
						<tr>
							<td>-</td>
							<td>-</td>
							<td>-</td>
							<td>-</td>
							<td>-</td>
							<td>-</td>
						</tr>
					<?php } else { ?>
						<?php foreach ($pastWinners AS $pastWinner) { ?>
							<tr>
								<td><?php echo $pastWinner['cID'] ?></td>
								<td><?php echo $pastWinner['uUser'] ?></td>
								<td><?php echo $pastWinner['Title'] ?></td>
								<td><?php echo $pastWinner['Description'] ?></td>
								<td><a href="<?php echo $pastWinner['RepoUrl'] ?>" target="_blank"><img src="<?php echo $pastWinner['Repo'] ?>.png" width="90px"/></a></td>
								<td><?php echo $pastWinner['Votes'] ?></td>
							</tr>
						<?php } ?>
					<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<div id="modal"></div>
<div id="dialog">
<div class="close" onclick="toggleModal()">X</div>
	<div id="forum"></div>
</div>
<footer>
<span>&copy; 2014 Node Academy<br/>Developed by <a href="http://effakt.info/">EffakT Development</a></span>
</footer>
</div>
<script type="text/javascript" src="console.js"></script>
<?php if ($isNextComp) { ?>
<script>
// set the date we're counting down to
var target_date = new Date("<?php echo $nextComp ?>").getTime();
 
// variables for time units
var days, hours, minutes, seconds;
 
// get tag element
var countdown = document.getElementById("countdown");
 
// update the tag with id "countdown" every 1 second
setInterval(function () {
 
	var d = new Date();
	var localTime = d.getTime();
	var localOffset = d.getTimezoneOffset() * 60000;
	var utc = localTime + localOffset;
	var offset = 12;   
	var newzealand = utc + (3600000*offset);

	var current_date = new Date(newzealand).getTime();
    var seconds_left = (target_date - current_date) / 1000;
 
    days = parseInt(seconds_left / 86400);
    seconds_left = seconds_left % 86400;
     
    hours = parseInt(seconds_left / 3600);
    seconds_left = seconds_left % 3600;
     
    minutes = parseInt(seconds_left / 60);
    seconds = parseInt(seconds_left % 60);
	
    countdown.innerHTML = "<b>" + days + "d, " + hours + "h, "
    + minutes + "m, " + seconds + "s</b>";  
 
}, 1000);
</script>
<?php } ?>
</body>
</html>