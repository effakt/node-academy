<?php
	include 'includes/access.inc.php';
	include 'includes/db.inc.php';
	try {
		$result = $pdo->prepare("SELECT Name FROM topics WHERE ID = :id");
		$result->bindParam(":id", $_GET['i']);
		$result->execute();
	} catch (PDOException $e) {
		echo "Error: unable to fetch topic name.";
	}
	$topicName = $result->fetchColumn();
	try {
		$result = $pdo->prepare("SELECT t.ID, t.Content, t.Date, t.Category, u.DisplayName AS Author, u.ID AS AuthorId, md5(u.Email) AS DisplayPic, u.Role AS Role FROM topics t INNER JOIN users u ON u.ID = t.Author WHERE t.ID = :id");
		$result->bindParam(":id", $_GET['i']);
		$result->execute();
	} catch (PDOException $e) {
		echo "Error: unable to fetch thread information";
	}
	$topicInfo = $result->fetch();
	try {
		$result = $pdo->prepare("SELECT um.Medal FROM usermedals um WHERE um.User = :id");
		$result->bindParam(":id", $topicInfo['AuthorId']);
		$result->execute();
	} catch (PDOException $e) {
		echo "Error getting topic user medals";
	}
	array_push($topicInfo, $result->fetchAll());
	try {
		$result = $pdo->prepare("SELECT p.ID, p.Content, p.Date, u.DisplayName AS Author, u.ID AS AuthorId, md5(u.Email) AS DisplayPic, u.Role AS Role FROM posts p INNER JOIN users u ON u.ID = p.Author WHERE p.Topic = :id");
		$result->bindParam(":id", $_GET['i']);
		$result->execute();
	} catch (PDOException $e) {
		echo "Error: unable to fetch posts";
	}
	$posts = $result->fetchAll();
	for ($i = 0; $i < count($posts); $i++) {
		try {
			$result = $pdo->prepare("SELECT um.Medal FROM usermedals um WHERE um.User = :id");
			$result->bindParam(":id", $posts[$i]['AuthorId']);
			$result->execute();
		} catch (PDOException $e) {
			echo "Error getting post user medals";
		}
		array_push($posts[$i], $result->fetchAll());
	}
	include 'viewTopic.html.php';
?>