<?php
	function exception($pdoErr, $e) {
		include 'db.inc.php';
		$state = $pdoErr[0];
		$code = $pdoErr[1];
		$message = $pdoErr[2];
		$line = $e->getLine();
		$file = $e->getFile();
		
		try {
			$result = $pdo->prepare("INSERT INTO errors (State, Code, Message, Line, File) VALUES (:state, :code, :message, :line, :file)");
			$result->bindParam(":state", $state);
			$result->bindParam(":code", $code);
			$result->bindParam(":message", $message);
			$result->bindParam(":line", $line);
			$result->bindParam(":file", $file);
			$result->execute();
		} catch (PDOException $e) {
			echo "Error: Unable to log error<br/>";
		}
	}
?>