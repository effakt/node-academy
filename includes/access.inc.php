<?php
	ini_set('session.cookie_lifetime', 60 * 60 * 24 * 30 * 12);
	if (session_status() == PHP_SESSION_NONE) {
		session_start();
	}
	include 'db.inc.php';
	include 'pdoErrorHandler.inc.php';
	include 'phpErrorHandler.inc.php';
	
	if (isset($_SESSION['u']) && !empty($_SESSION['u'])) {
		try {
			$result = $pdo->prepare("SELECT Active from users WHERE Name = :name");
			$result->bindParam(":name",$_SESSION['u']);
			$result->execute();
			$isUserActive = (bool)$result->fetchColumn();
		} catch (PDOException $e) {
			exception($result->errorInfo(), $e);
			die("Unable to fetch user active bool.");
		}
		try {
			$result = $pdo->prepare("SELECT ID FROM users WHERE Name = :name");
			$result->bindParam(":name", $_SESSION['u']);
			$result->execute();
			$userId = $result->fetchColumn();
		} catch (PDOException $e) {
			exception($result->errorInfo(), $e);
			die("Unable to fetch user id");
		}
		try {
			$result = $pdo->prepare("SELECT Role FROM users WHERE ID = :id");
			$result->bindParam(":id", $userId);
			$result->execute();
			$userRole = $result->fetchColumn();
		} catch (PDOException $e) {
			exception($result->errorInfo(), $e);
			die("Unable to fetch user role");
		}
		try {
			$result = $pdo->prepare("SELECT MD5(Email) FROM users WHERE ID = :id");
			$result->bindParam(":id", $userId);
			$result->execute();
			$userDisplayPic = $result->fetchColumn();
		} catch (PDOException $e) {
			exception($result->errorInfo(), $e);
			die("Unable to fetch user email");
		}
	}
?>