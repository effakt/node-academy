<?php
	include 'db.inc.php';
	$error_log = $_SERVER['DOCUMENT_ROOT'].'/../php/logs/php_error_log';
	$log = file_get_contents($error_log);
	if (!empty($log)) {
		file_put_contents($error_log, "");
		$logs = array_filter(explode("\n",$log));
		$match = [];
		for($i = 0; $i < count($logs); $i++) {
			$match[$i] = [];
			preg_match("/\[([^\s]*\s[^\s]*) .*] ((.*) in (.*) on line (.*))/", $logs[$i], $matches);
			for($y = 1; $y < count($matches); $y++) {
				$matches[$y] = trim($matches[$y]);
				array_push($match[$i], trim($matches[$y]));
			}
		}
		$sql = "INSERT INTO errors (Message, Line, File, Date) VALUES ";
		$i = 0;
		if (count($match) > 0) {
			foreach ($match AS $mat) {
				$date = date("Y-m-d H:i:s", strtotime($mat[0]));
				$sql .= " ('".addslashes($mat['1'])."','".addslashes($mat['4'])."','".addslashes($mat['3'])."','".$date."')";
				if ($i < count($match)-1)
					$sql .= ",";
				$i++;
			}
			try {
				$result = $pdo->query($sql);
			} catch (PDOException $e) {
				exception($result->errorInfo(), $e);
				die();
			}
		}
	}
?>