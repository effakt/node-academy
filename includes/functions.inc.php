<?php
	function pass($pass) {
		$pass = sha1($pass);
		$salts = ["Node","JS","Academy"];
		foreach ($salts AS $salt) {
			$pass = sha1($pass.$salt);
		}
		return $pass;
	}
	function timeago($time) {
	$etime = time() - $time;
	if ($etime < 1) {
		return '0 seconds';
	}
	$interval = array(12*30*24*60*60 => "year", 30*24*60*60 => "month", 7*24*60*60 => "week", 24*60*60 => "day", 60*60 => "hour", 60 => "minute", 1 => "second");
	foreach ($interval as $secs => $str) {
		$d = $etime / $secs;
		if ($d >= 1) {
			$r = round($d);
			return $r.' '.$str.($r>1?'s':'').' ago';
		}
	}
}
?>