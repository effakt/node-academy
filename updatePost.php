<?php
	include 'includes/access.inc.php';
	include 'includes/db.inc.php';
	if ($_POST['a'] == "topic") {
		if (!isset($_POST['content']) || empty($_POST['content'])) {
			die("Error: Content empty");
		} else {
			try {
				$result = $pdo->prepare("UPDATE  `topics` SET  `Content` =  :content WHERE `ID` = :id;");
				$result->bindParam(":content", $_POST['content']);
				$result->bindParam(":id", $_POST['id']);
				$result->execute();
				echo "Success";
			} catch (PDOException $e) {
				exception($result->errorInfo(), $e);
				die("Error: Unable to update topic");
			}
		}
	} elseif($_POST['a'] == "post") {
		if (!isset($_POST['content']) || empty($_POST['content'])) {
			die("Error: Content empty");
		} else {
			try {
				$result = $pdo->prepare("UPDATE  `posts` SET  `Content` = :content WHERE  `ID` = :id;");
				$result->bindParam(":content", $_POST['content']);
				$result->bindParam(":id", $_POST['id']);
				$result->execute();
				echo "Success";
			} catch (PDOException $e) {
				exception($result->errorInfo(), $e);
				die("Error: Unable to update post");
			}
		}
	}
?>