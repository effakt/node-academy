<?php
	include 'includes/access.inc.php';
	include 'includes/db.inc.php';
	include 'includes/functions.inc.php';
	
	try {
		$result = $pdo->prepare("SELECT um.Medal FROM usermedals um INNER JOIN users u ON um.User = u.ID WHERE u.Name = :name");
		$result->bindParam(":name", $_SESSION['u']);
		$result->execute();
		$medals = $result->fetchAll();
	} catch (PDOException $e) {
		exception($result->errorInfo(), $e);
		die("Unable to fetch user medals");
	}
		
	include 'profile.html.php';
?>