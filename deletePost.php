<?php
	include 'includes/access.inc.php';
	include 'includes/db.inc.php';
	if ($_POST['a'] == "topic") {
			try {
				$result = $pdo->prepare("DELETE FROM `topics` WHERE `ID` = :id");
				$result->bindParam(":id", $_POST['id']);
				$result->execute();
				echo "Success";
			} catch (PDOException $e) {
				exception($result->errorInfo(), $e);
				die("Error: Unable to delete topic");
			}
		} elseif($_POST['a'] == "post") {
			try {
				$result = $pdo->prepare("DELETE FROM `posts` WHERE `ID` = :id");
				$result->bindParam(":id", $_POST['id']);
				$result->execute();
				echo "Success";
			} catch (PDOException $e) {
				exception($result->errorInfo(), $e);
				die("Error: Unable to delete post");
			}
		}
?>